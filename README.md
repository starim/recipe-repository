# Recipe Repository

A searchable repository of recipes.

License: AGPLv3

To run this project:
* run `rails s` from the server folder
* run `npm run serve` from the client folder

Deploying
--

`cd` to the server and run `bundle install` before running the following commands.

`mina setup` to create folder structure on the server, only necessary if this is the first time deploying to the server
`mina deploy` to deploy and start up the app


Starting the Puma Service
--

If the server hosting the Recipe Repository goes down, Puma won't be started automatically when it comes back online. To start the Repository, first become the `recipe_repository` user, then:

```
sudo -i -u recipe_repository
cd /srv/http/recipe-repository/current/server
RAILS_ENV=production bundle exec puma -q -d -e production -C config/puma.rb
```

Installing rbenv
--

On the server, install these two AUR packages:
* [rbenv](https://aur.archlinux.org/packages/rbenv)
* [ruby-build](https://aur.archlinux.org/packages/ruby-build)

Then add this line to the .bashrc of the user on the server that will run the
application:
```
eval "$(rbenv init -)"
```

Then open a new terminal session as that user and run
```
rbenv install 3.0.3
```
or whichever Ruby version is currently specified in this project's `server/.ruby-version`

