source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '>=3.0.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 7.1.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 6.4.1'

# irb is a gem now instead of being packaged with Ruby
gem 'irb'

# This is to prevent deployments from crashing with a LoadError "Cannot load
# such file -- rdoc" when including irb in the Gemfile. Possibly caused by this
# irb bug:
# https://github.com/ruby/irb/issues/342
gem 'rdoc'

gem 'json', '~> 2.7.1'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
#gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Reduces boot times through caching; required in config/boot.rb
#gem 'bootsnap', '>= 1.1.0', require: false

# this gem is required in all environments because it's used in the seeds file
gem 'factory_bot_rails', '~> 6.4.3'
gem 'active_model_serializers', '~> 0.10'

# Ruby 3.1 uses Psych 4 as a default gem, which has breaking changes that cause
# Rails to error on startup. Forcing a version lower than 4 allows Rails 6 to
# start up on Ruby 3.1.
gem 'psych', '< 4'

# enable CORS
gem 'rack-cors', '~> 1.1'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 6.1.0'
  gem 'awesome_print', '~> 1.8'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'annotate', '~> 3.0'

  # Mina is our deployment tool
  gem 'mina', '~> 1.2.5'
  gem 'mina-puma', '~> 1.1.0', require: false
end

group :test do
  # currently no stable release of shoulda-matchers supports Rails 5, but the
  # master branch does
  gem 'shoulda-matchers', '~> 4.2'
  gem 'database_cleaner-active_record', '~> 2.1.0'
  gem 'json_schema', '~> 0.20'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
