# == Schema Information
#
# Table name: recipes
#
#  id          :bigint(8)        not null, primary key
#  name        :text             not null
#  description :text             default(""), not null
#  source      :text
#  directions  :text             not null, is an Array
#  comments    :text             default(""), not null
#  category_id :bigint(8)        not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class RecipeSerializer < ActiveModel::Serializer
  attributes :name, :description, :source, :directions, :comments

  belongs_to :category do
    object.category.name
  end

  has_many :ingredient_preparations

  attribute :tags do
    object.tags.map(&:name)
  end
end
