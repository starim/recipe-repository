# == Schema Information
#
# Table name: ingredients
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class IngredientSerializer < ActiveModel::Serializer
  attributes :id, :name
  attribute :num_recipes, if: :num_recipes_set?

  def num_recipes_set?
    object.num_recipes
  end
end
