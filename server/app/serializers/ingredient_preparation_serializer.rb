# == Schema Information
#
# Table name: ingredient_preparations
#
#  id            :bigint(8)        not null, primary key
#  preparation   :text
#  amount        :decimal(, )
#  recipe_id     :bigint(8)
#  ingredient_id :bigint(8)
#  unit_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class IngredientPreparationSerializer < ActiveModel::Serializer
  attributes :amount, :preparation

  belongs_to :ingredient do
    object.ingredient.name
  end

  belongs_to :unit do
    object.unit&.name
  end
end
