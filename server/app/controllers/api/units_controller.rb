module Api
  class UnitsController < ApplicationController
    def index
      render json: Unit.all
    end
  end
end
