module Api
  class RecipesController < ApplicationController
    def index
      recipes =
        Recipe
        .includes(:category, :tags, ingredient_preparations: [:ingredient, :unit])
        .name_like(params["recipe_name_filter"])
        .category_name_like(params["category_filter"])
        .ingredient_name_like(params["ingredient_filter"])
        .tag_name_like(params["tag_filter"])
        .order(:updated_at)
      render(
        json: recipes,
        include: [
          'category',
          'tags',
          'ingredient_preparations.ingredient',
          'ingredient_preparations.unit',
        ],
      )
    end

    def show
      recipe =
        Recipe
        .includes(:category, :tags, ingredient_preparations: [:ingredient, :unit])
        .find_by( name: params[:id] )
      if recipe.nil?
        head :not_found
      else
        render(
          json: recipe,
          include: [
            'category',
            'tags',
            'ingredient_preparations.ingredient',
            'ingredient_preparations.unit',
          ],
        )
      end
    end

    def create
      begin
        recipe = Recipe.new allowed_recipe_params
        ActiveRecord::Base.transaction do
          update_recipe_associations!(recipe, params)
          recipe.save!
        end
        head :created
      rescue ActiveRecord::RecordInvalid => error
        # if the Recipe model didn't trip the validation error then it hasn't
        # been checked for validity
        if !error.record.is_a? Recipe
          recipe.valid?
        end
        log_error(
          "Failed model validations during recipe#create: #{recipe.errors}",
          params,
          error,
        )
        render json: { errors: recipe.errors.full_messages }, status: :bad_request
      rescue ActionController::ParameterMissing => error
        log_error(
          "Missing required parameter to recipe#create",
          params,
          error,
        )
        render json: { errors: [error.message] }, status: :bad_request
      rescue => error
        log_error(
          "Unexpected error in recipe#create",
          params,
          error,
        )
        render json: { errors: [error.message] }, status: :internal_server_error
      end
    end

    def update
      recipe = Recipe.find_by( name: params[:id] )
      if recipe.nil?
        head :not_found
      else
        begin
          recipe.attributes = allowed_recipe_params
          ActiveRecord::Base.transaction do
            remove_recipe_associations!(recipe, params)
            update_recipe_associations!(recipe, params)
            recipe.save!
          end
          head :no_content
        rescue ActiveRecord::RecordInvalid => error
          # if the Recipe model didn't trip the validation error then it hasn't
          # been checked for validity
          if !error.record.is_a? Recipe
            recipe.valid?
          end
          log_error(
            "Failed model validations during recipe#update: #{recipe.errors}",
            params,
            error,
          )
          render json: { errors: recipe.errors.full_messages },
            status: :bad_request
        rescue ActionController::ParameterMissing => error
          log_error(
            "Missing required parameter to recipe#update",
            params,
            error,
          )
          render json: { errors: [error.message] }, status: :bad_request
        rescue => error
          log_error(
            "Unexpected error in recipe#update",
            params,
            error,
          )
          render json: { errors: [error.message] }, status: :internal_server_error
        end
      end
    end

    def destroy
      recipe = Recipe.find_by(name: params[:id])
      if recipe.nil?
        head :not_found
      else
        if recipe.destroy
          head :no_content
        else
          log_error(
            "Unable to destroy recipe in recipe#destroy. Failed validations: #{recipe.errors}",
            params,
            nil,
          )
          render json: { errors: recipe.errors.full_messages },
            status: :internal_server_error
        end
      end
    end

    private

    def allowed_recipe_params
      params.require(:recipe).permit(
        :name,
        :description,
        :image,
        :source,
        :comments,
        directions: [],
      )
    end

    def allowed_ingredient_prep_params(ingredient_prep_params)
      ingredient_prep_params.permit(
        :preparation,
        :amount,
      )
    end

    # takes a Recipe model and a params hash and updates the Recipe's
    # associations based on the params provided, creating new Ingredient, Unit,
    # or Category models if needed
    def update_recipe_associations!(recipe, params)
      if params[:recipe][:category]
        recipe.category =
          Category
          .where( name: params[:recipe][:category] )
          .first_or_create!
      end

      ingredient_preparations =
        (params[:recipe][:ingredient_preparations] || []).reject do |prep_params|
          prep_params.values.all? {|value| value.blank? }
        end
      if !ingredient_preparations.empty?
        ingredient_preparations.map do |prep_params|
          ingredient =
            Ingredient
            .where( name: prep_params[:ingredient] )
            .first_or_create!
          ingredient_preparation =
            IngredientPreparation
            .where( recipe: recipe, ingredient: ingredient )
            .first_or_initialize
          if prep_params[:unit] && !prep_params[:unit].blank?
            unit = Unit.where( name: prep_params[:unit] ).first_or_create!
            ingredient_preparation.unit = unit
          end
          ingredient_preparation.attributes = allowed_ingredient_prep_params(prep_params)
          ingredient_preparation.recipe = recipe
          ingredient_preparation.save!
          ingredient_preparation
        end
      end

      if params[:recipe][:taggings]
        recipe.taggings = params[:recipe][:taggings].map do |tag_params|
          tag =
            Tag
            .where( name: tag_params[:tag] )
            .first_or_create!
          tagging =
            Tagging
            .where( recipe: recipe, tag: tag )
            .first_or_initialize
          tagging.recipe = recipe
          tagging
        end
      end
    end

    # takes a Recipe model and a params hash and removes any of the Recipe's
    # associations that are missing from the params provided
    def remove_recipe_associations!(recipe, params)
      ingredient_names =
        (params[:recipe][:ingredient_preparations] || []).reject do |prep_params|
          prep_params.values.all? {|value| value.blank? }
        end
        .map { |prep_params| prep_params[:ingredient] }
      recipe.ingredient_preparations.reject do |preparation|
        ingredient_names.include? preparation.ingredient.name
      end.each { |preparation| preparation.destroy! }

      tag_names = (params[:recipe][:taggings] || []).map do |tag_params|
        tag_params[:tag]
      end
      recipe.taggings.reject do |tagging|
        tag_names.include? tagging.tag.name
      end.each { |tagging| tagging.destroy! }
    end
  end
end
