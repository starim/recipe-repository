module Api
  class TagsController < ApplicationController
    def index
      tags = Tag.order(:name)
      render(
        json: tags
      )
    end

    def show
      tag =
        Tag.find_by( name: params[:id] )
      if tag.nil?
        head :not_found
      else
        render(
          json: tag
        )
      end
    end

    def create
      begin
        tag = Tag.new allowed_tag_params
        tag.save!
        head :created
      rescue ActiveRecord::RecordInvalid => error
        log_error(
          "Missing required parameter to tag#create",
          params,
          error,
        )
        render json: { errors: tag.errors }, status: :bad_request
      rescue ActionController::ParameterMissing => error
        render json: { errors: [error.message] }, status: :bad_request
      rescue => error
        log_error(
          "Unexpected error in tags#create",
          params,
          error,
        )
        render json: { errors: [error.message] }, status: :internal_server_error
      end
    end

    def destroy
      tag = Tag.find_by(name: params[:id])
      if tag.nil?
        head :not_found
      else
        if tag.destroy
          head :no_content
        else
          log_error(
            "Unexpected error in tags#destroy",
            params,
            error,
          )
          render json: { errors: tag.errors }, status: :internal_server_error
        end
      end
    end

    private

    def allowed_tag_params
      params.require(:tag).permit(
        :id,
        :name
      )
    end

  end
end
