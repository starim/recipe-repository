module Api
  class IngredientsController < ApplicationController
    def index
      if params["most_used_count"]
        render json: Ingredient.most_used(params["most_used_count"])
      else
        render json: Ingredient.all
      end
    end
  end
end
