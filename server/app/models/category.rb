# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  belongs_to :parent,
    class_name: 'Category', foreign_key: 'parent_id', optional: true
  has_many :children, class_name: 'Category', inverse_of: :parent
  has_many :recipes, inverse_of: :category
end
