# == Schema Information
#
# Table name: ingredients
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Ingredient < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :ingredient_preparations, inverse_of: :ingredient
  has_many :recipes, through: :ingredient_preparations, inverse_of: :ingredients

  # This is a performance enhancement to allow the most_used() method to save
  # the number of associated recipes on each Ingredient without having to do a
  # whole extra DB looking the way ingredient.recipes.length would.
  attr_accessor :num_recipes

  # This method returns the count most used ingredients in recipes ordered from
  # most used to least. Any ingredients that have never been used in any recipe
  # are ignored by this method.
  def self.most_used(count)
    results = Ingredient
      .joins(:recipes)
      .group(:id)
      .order("connections desc")
      .limit(count)
      .pluck(Arel.sql("count(DISTINCT recipes.id) as connections"), Arel.sql("ingredients.*"))

    results.map! do |row|
      num_recipes = row.shift
      ingredient_properties =
        Ingredient
        .columns
        .map {|column| column.name}
        .zip(row)
        .to_h

      ingredient = Ingredient.new(ingredient_properties)
      ingredient.num_recipes = num_recipes
      ingredient
    end
  end
end
