# == Schema Information
#
# Table name: tags
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  has_many :taggings
  has_many :recipes, through: :taggings, inverse_of: :tags

  before_validation :convert_to_lower_case

  private

  def convert_to_lower_case
    self&.name = self&.name&.downcase
  end
end
