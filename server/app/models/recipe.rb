# == Schema Information
#
# Table name: recipes
#
#  id          :bigint(8)        not null, primary key
#  name        :text             not null
#  description :text             default(""), not null
#  source      :text
#  directions  :text             not null, is an Array
#  comments    :text             default(""), not null
#  category_id :bigint(8)        not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Recipe < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :description, length: { minimum: 0, allow_nil: false, message: "can't be nil" }
  validates_length_of :directions, minimum: 1, too_short: "must have at least one step"

  belongs_to :category
  has_many :ingredient_preparations, inverse_of: :recipe, dependent: :destroy
  has_many :ingredients,
    through: :ingredient_preparations, inverse_of: :recipes
  has_many :taggings, inverse_of: :recipe, dependent: :destroy
  has_many :tags, through: :taggings, inverse_of: :recipes

  has_one_attached :image

  before_validation :convert_blank_source_to_nil, :remove_blank_directions,
    :convert_nil_description_to_empty_string,
    :convert_nil_directions_to_empty_array

  def self.name_like(name)
    if name.present?
      where("recipes.name ILIKE ?", "%#{name}%")
    else
      all
    end
  end

  def self.category_name_like(name)
    if name.present?
      subquery = Category
        .select("id")
        .where("categories.name ILIKE ?", "%#{name}%")
      where(category: subquery)
    else
      all
    end
  end

  def self.ingredient_name_like(name)
    if name.present?
      subquery = IngredientPreparation
        .select("recipe_id")
        .joins(:ingredient)
        .where("ingredients.name ILIKE ?", "%#{name}%")
      where(id: subquery)
    else
      all
    end
  end

  def self.tag_name_like(name)
    if name.present?
      joins(
        "INNER JOIN taggings on taggings.recipe_id=recipes.id
        INNER JOIN tags ON tags.id=taggings.tag_id"
      ).where("tags.name ILIKE ?", "%#{name}%")
    else
      all
    end
  end

  private

  def convert_nil_description_to_empty_string
    if self.description.nil?
      self.description = ""
    end
  end

  def convert_nil_directions_to_empty_array
    if self.directions.nil?
      self.directions = []
    end
  end

  def convert_blank_source_to_nil
    if self&.source&.blank?
      self.source = nil
    end
  end

  def remove_blank_directions
    self&.directions&.select! { |direction| !direction.blank? }
  end
end
