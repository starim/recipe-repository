# == Schema Information
#
# Table name: ingredient_preparations
#
#  id            :bigint(8)        not null, primary key
#  preparation   :text
#  amount        :decimal(, )
#  recipe_id     :bigint(8)
#  ingredient_id :bigint(8)
#  unit_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class IngredientPreparation < ApplicationRecord
  belongs_to :recipe
  belongs_to :ingredient
  belongs_to :unit, optional: true

  before_validation :convert_blank_preparation_to_nil

  private

  def convert_blank_preparation_to_nil
    if self.preparation.blank?
      self.preparation = nil
    end
  end
end
