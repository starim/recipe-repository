# == Schema Information
#
# Table name: units
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Unit < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :ingredient_preparations, inverse_of: :unit
end
