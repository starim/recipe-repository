# == Schema Information
#
# Table name: taggings
#
#  id         :bigint(8)        not null, primary key
#  recipe_id  :bigint(8)        not null
#  tag_id     :bigint(8)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tagging < ApplicationRecord
  # https://github.com/thoughtbot/shoulda-matchers/issues/814
  validates_uniqueness_of :recipe, scope: :tag_id

  belongs_to :recipe
  belongs_to :tag
end
