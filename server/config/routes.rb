Rails.application.routes.draw do
  namespace :api, constraints: { format: 'json' } do
    get 'health_check', to: 'health_check#health_check'
    resources :recipes, id: /[^\/]+/
    resources :tags
    resources :categories, only: :index
    resources :ingredients, only: :index
    resources :units, only: :index
  end
end
