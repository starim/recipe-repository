class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string  :name,  null: false
      t.timestamps
    end
    add_index :tags, :name, unique: true

    create_table :taggings do |t|
      t.references :recipe, foreign_key: true, null: false
      t.references :tag, foreign_key: true, null: false
      t.timestamps
    end
    add_index :taggings, [:recipe_id, :tag_id], unique: true
  end
end
