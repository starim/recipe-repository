class CreateBaseTables < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.text :name, null: false
      t.integer :parent_id, foreign_key: true

      t.timestamps
    end
    add_index :categories, :name, unique: true

    create_table :ingredients do |t|
      t.text :name, null: false

      t.timestamps
    end
    add_index :ingredients, :name, unique: true

    create_table :units do |t|
      t.text :name, null: false

      t.timestamps
    end
    add_index :units, :name, unique: true

    create_table :recipes do |t|
      t.text :name, null: false
      t.text :description, null: false, default: ""
      t.text :image
      t.text :source
      t.text :directions, array: true, null: false
      t.text :comments, array: true, null: false, default: []
      t.references :category, foreign_key: true, null: false

      t.timestamps
    end
    add_index :recipes, :name, unique: true

    create_table :ingredient_preparations do |t|
      t.text :preparation
      t.decimal :amount, null: false
      t.references :recipe, foreign_key: true
      t.references :ingredient, foreign_key: true
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
