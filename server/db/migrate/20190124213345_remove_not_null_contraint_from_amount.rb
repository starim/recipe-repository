class RemoveNotNullContraintFromAmount < ActiveRecord::Migration[5.2]
  def change
    change_column :ingredient_preparations, :amount, :decimal, :null => true
  end
end
