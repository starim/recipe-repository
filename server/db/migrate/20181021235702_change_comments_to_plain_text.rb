class ChangeCommentsToPlainText < ActiveRecord::Migration[5.2]
  def up
    # fetch all comments and squash them into a single text block
    comments = Recipe.where.not( comments: nil).map do |recipe|
      { id: recipe.id, comments: recipe.comments.join("\n") }
    end

    # update the column to be a text field
    change_column :recipes, :comments, :text, null: false, default: ""

    # write all the data back
    comments.each do |comment_data|
      recipe = Recipe.find_by(id: comment_data[:id])
      recipe.comments = comment_data[:comments]
      recipe.save!
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
