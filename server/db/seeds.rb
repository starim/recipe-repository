ActiveRecord::Base.transaction do

  # categories
  food = Category.where( name: "Food" ).first_or_create!
  Category.where( name: "Drink" ).first_or_create!

  # tags
  breakfast = Tag.where( name: "Breakfast" ).first_or_create!

  # units
  Unit.where( name: "L" ).first_or_create!
  Unit.where( name: "mL" ).first_or_create!
  Unit.where( name: "gal" ).first_or_create!
  Unit.where( name: "quart" ).first_or_create!
  cup = Unit.where( name: "cup" ).first_or_create!
  oz = Unit.where( name: "oz" ).first_or_create!
  tbsp = Unit.where( name: "tbsp" ).first_or_create!
  tsp = Unit.where( name: "tsp" ).first_or_create!

  # ingredients
  tortilla = Ingredient.where( name: "tortilla" ).first_or_create!
  cheddar_cheese = Ingredient.where( name: "cheddar cheese" ).first_or_create!
  green_bell_pepper = Ingredient.where( name: "green bell pepper" ).first_or_create!
  egg = Ingredient.where( name: "egg" ).first_or_create!
  chorizo = Ingredient.where( name: "chorizo" ).first_or_create!
  tater_tots = Ingredient.where( name: "tater tots" ).first_or_create!
  romaine_lettuce = Ingredient.where( name: "romaine lettuce" ).first_or_create!
  milk = Ingredient.where( name: "milk" ).first_or_create!
  apple_sauce = Ingredient.where( name: "apple sauce" ).first_or_create!
  cinnamon = Ingredient.where( name: "cinnamon" ).first_or_create!
  vanilla_extract = Ingredient.where( name: "vanilla extract" ).first_or_create!
  pancake_mix = Ingredient.where( name: "pancake mix" ).first_or_create!
  brown_sugar = Ingredient.where( name: "brown sugar" ).first_or_create!
  water = Ingredient.where( name: "water" ).first_or_create!
  butter = Ingredient.where( name: "butter" ).first_or_create!

  # recipes
  breakfast_burritos = Recipe.where(
    name: "Brent's Breakfast Burritos",
    description: "Makes two meaty breakfast burritos.",
    directions: [
      "Thaw chorizo.",
      "Heat skillet to medium heat and add chorizo.",
      "Open the tortillas and fill with lettuce, bell pepper, cheese, and tater tots.",
      "Cook chorizo until browned, stirring occasionally.",
      "Fill tortillas with chorizo.",
      "Crack eggs into skillet, and avoid breaking yolk. Cook 2 minutes on one side then flip each egg and continue cooking an additional minute.",
      "Remove skillet from heat and add eggs to tortillas.",
      "Using a fork, scramble the ingredients together inside each tortilla. Use the tines of the fork to break apart the eggs and tater tots.",
      "Fold each tortilla into a wrap and serve.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tortilla,
    preparation: nil,
    amount: 2,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: cheddar_cheese,
    preparation: "shredded",
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: green_bell_pepper,
    preparation: "diced",
    amount: 0.5,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: egg,
    preparation: nil,
    amount: 4,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: chorizo,
    preparation: "cooked until browned",
    amount: 8,
    unit: oz,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tater_tots,
    preparation: nil,
    amount: 16,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: romaine_lettuce,
    preparation: "chopped",
    amount: 4,
    unit: oz,
  ).first_or_create!

  Tagging.where(
    recipe: breakfast_burritos,
    tag: breakfast,
  ).first_or_create!

  breakfast_burritos2 = Recipe.where(
    name: "Brent's Breakfast Burritos 2",
    description: "Makes two meaty breakfast burritos.",
    directions: [
      "Thaw chorizo.",
      "Heat skillet to medium heat and add chorizo.",
      "Open the tortillas and fill with lettuce, bell pepper, cheese, and tater tots.",
      "Cook chorizo until browned, stirring occasionally.",
      "Fill tortillas with chorizo.",
      "Crack eggs into skillet, and avoid breaking yolk. Cook 2 minutes on one side then flip each egg and continue cooking an additional minute.",
      "Remove skillet from heat and add eggs to tortillas.",
      "Using a fork, scramble the ingredients together inside each tortilla. Use the tines of the fork to break apart the eggs and tater tots.",
      "Fold each tortilla into a wrap and serve.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tortilla,
    preparation: nil,
    amount: 2,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: cheddar_cheese,
    preparation: "shredded",
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: green_bell_pepper,
    preparation: "diced",
    amount: 0.5,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: egg,
    preparation: nil,
    amount: 4,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: chorizo,
    preparation: "cooked until browned",
    amount: 8,
    unit: oz,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tater_tots,
    preparation: nil,
    amount: 16,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: romaine_lettuce,
    preparation: "chopped",
    amount: 4,
    unit: oz,
  ).first_or_create!

  Tagging.where(
    recipe: breakfast_burritos,
    tag: breakfast,
  ).first_or_create!

  breakfast_burritos3 = Recipe.where(
    name: "Brent's Breakfast Burritos 3",
    description: "Makes two meaty breakfast burritos.",
    directions: [
      "Thaw chorizo.",
      "Heat skillet to medium heat and add chorizo.",
      "Open the tortillas and fill with lettuce, bell pepper, cheese, and tater tots.",
      "Cook chorizo until browned, stirring occasionally.",
      "Fill tortillas with chorizo.",
      "Crack eggs into skillet, and avoid breaking yolk. Cook 2 minutes on one side then flip each egg and continue cooking an additional minute.",
      "Remove skillet from heat and add eggs to tortillas.",
      "Using a fork, scramble the ingredients together inside each tortilla. Use the tines of the fork to break apart the eggs and tater tots.",
      "Fold each tortilla into a wrap and serve.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tortilla,
    preparation: nil,
    amount: 2,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: cheddar_cheese,
    preparation: "shredded",
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: green_bell_pepper,
    preparation: "diced",
    amount: 0.5,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: egg,
    preparation: nil,
    amount: 4,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: chorizo,
    preparation: "cooked until browned",
    amount: 8,
    unit: oz,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tater_tots,
    preparation: nil,
    amount: 16,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: romaine_lettuce,
    preparation: "chopped",
    amount: 4,
    unit: oz,
  ).first_or_create!

  Tagging.where(
    recipe: breakfast_burritos,
    tag: breakfast,
  ).first_or_create!

  breakfast_burritos4 = Recipe.where(
    name: "Brent's Breakfast Burritos 4",
    description: "Makes two meaty breakfast burritos.",
    directions: [
      "Thaw chorizo.",
      "Heat skillet to medium heat and add chorizo.",
      "Open the tortillas and fill with lettuce, bell pepper, cheese, and tater tots.",
      "Cook chorizo until browned, stirring occasionally.",
      "Fill tortillas with chorizo.",
      "Crack eggs into skillet, and avoid breaking yolk. Cook 2 minutes on one side then flip each egg and continue cooking an additional minute.",
      "Remove skillet from heat and add eggs to tortillas.",
      "Using a fork, scramble the ingredients together inside each tortilla. Use the tines of the fork to break apart the eggs and tater tots.",
      "Fold each tortilla into a wrap and serve.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tortilla,
    preparation: nil,
    amount: 2,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: cheddar_cheese,
    preparation: "shredded",
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: green_bell_pepper,
    preparation: "diced",
    amount: 0.5,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: egg,
    preparation: nil,
    amount: 4,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: chorizo,
    preparation: "cooked until browned",
    amount: 8,
    unit: oz,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tater_tots,
    preparation: nil,
    amount: 16,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: romaine_lettuce,
    preparation: "chopped",
    amount: 4,
    unit: oz,
  ).first_or_create!

  Tagging.where(
    recipe: breakfast_burritos,
    tag: breakfast,
  ).first_or_create!

  breakfast_burritos5 = Recipe.where(
    name: "Brent's Breakfast Burritos 5",
    description: "Makes two meaty breakfast burritos.",
    directions: [
      "Thaw chorizo.",
      "Heat skillet to medium heat and add chorizo.",
      "Open the tortillas and fill with lettuce, bell pepper, cheese, and tater tots.",
      "Cook chorizo until browned, stirring occasionally.",
      "Fill tortillas with chorizo.",
      "Crack eggs into skillet, and avoid breaking yolk. Cook 2 minutes on one side then flip each egg and continue cooking an additional minute.",
      "Remove skillet from heat and add eggs to tortillas.",
      "Using a fork, scramble the ingredients together inside each tortilla. Use the tines of the fork to break apart the eggs and tater tots.",
      "Fold each tortilla into a wrap and serve.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tortilla,
    preparation: nil,
    amount: 2,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: cheddar_cheese,
    preparation: "shredded",
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: green_bell_pepper,
    preparation: "diced",
    amount: 0.5,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: egg,
    preparation: nil,
    amount: 4,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: chorizo,
    preparation: "cooked until browned",
    amount: 8,
    unit: oz,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: tater_tots,
    preparation: nil,
    amount: 16,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: breakfast_burritos,
    ingredient: romaine_lettuce,
    preparation: "chopped",
    amount: 4,
    unit: oz,
  ).first_or_create!

  Tagging.where(
    recipe: breakfast_burritos,
    tag: breakfast,
  ).first_or_create!

  pancakes = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  pancakes2 = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes 2",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  pancakes3 = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes 3",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  pancakes4 = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes 4",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  pancakes4 = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes 4",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  pancakes5 = Recipe.where(
    name: "Devin's Apple Cinnamon Pancakes 5",
    description: "Handle with care.",
    directions: [
      "Mix dry ingredients in bowl.",
      "Add water, apple sauce, egg and vanilla extract.",
      "Add milk in small amounts, mixing until batter is desired thickness. More milk -> Thinner pancakes.",
      "If you feel the mix has become too thin, add mix to thickin.",
      "Pour out pancakes onto medium-high buttered pan.",
      "Dust top with cinnamon.",
      "Flip once pancakes are golden brown.",
      "Remove from pan once middle cooks through.",
      "Serve in stacks of three.",
    ],
    category: food,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
      ingredient: butter,
      preparation: nil,
      amount: 1,
      unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: water,
    preparation: nil,
    amount: 0.5,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: pancake_mix,
    preparation: nil,
    amount: 2,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: egg,
    preparation: nil,
    amount: 1,
    unit: nil,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: vanilla_extract,
    preparation: nil,
    amount: 1,
    unit: tsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: milk,
    preparation: nil,
    amount: 1,
    unit: cup,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: cinnamon,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: brown_sugar,
    preparation: nil,
    amount: 1,
    unit: tbsp,
  ).first_or_create!
  IngredientPreparation.where(
    recipe: pancakes,
    ingredient: apple_sauce,
    preparation: nil,
    amount: 3,
    unit: tbsp
  ).first_or_create!

  Tagging.where(
    recipe: pancakes,
    tag: breakfast,
  ).first_or_create!

end
