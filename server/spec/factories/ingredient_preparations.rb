# == Schema Information
#
# Table name: ingredient_preparations
#
#  id            :bigint(8)        not null, primary key
#  preparation   :text
#  amount        :decimal(, )
#  recipe_id     :bigint(8)
#  ingredient_id :bigint(8)
#  unit_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :ingredient_preparation do
    preparation { nil }
    amount { nil }
    recipe
    ingredient
    unit { nil }
  end
end
