# == Schema Information
#
# Table name: taggings
#
#  id         :bigint(8)        not null, primary key
#  recipe_id  :bigint(8)        not null
#  tag_id     :bigint(8)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :tagging do
    recipe
    tag
  end
end
