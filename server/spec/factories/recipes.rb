# == Schema Information
#
# Table name: recipes
#
#  id          :bigint(8)        not null, primary key
#  name        :text             not null
#  description :text             default(""), not null
#  source      :text
#  directions  :text             not null, is an Array
#  comments    :text             default(""), not null
#  category_id :bigint(8)        not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :recipe do
    sequence(:name) { |n| "Recipe ##{n}" }
    description { "" }
    directions {["Step 1"]}
    category

    transient do
      ingredients_count { 1 }
    end

    transient do
      tags_count { 1 }
    end

    after(:create) do |recipe, evaluator|
      create_list(:ingredient_preparation, evaluator.ingredients_count, recipe: recipe)
      create_list(:tagging, evaluator.tags_count, recipe: recipe)
    end
  end
end
