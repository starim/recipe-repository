require 'rails_helper'
require 'erb'

RSpec.describe "GET /api/recipes/:name", type: :request do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  let!(:recipe) { create :recipe }

  context 'when the given recipe name exists' do
    context "and the recipe name doesn't contain a ." do
      before do
        get "/api/recipes/#{ERB::Util.url_encode(recipe.name)}"
      end

      it 'returns the recipe data in the expected schema' do
        expect(response).to be_successful
        assert_response_schema
      end
    end

    context "and the recipe name contains a ." do
      let!(:recipe) { create :recipe, name: "Pimm's No. 1" }

      before do
        get "/api/recipes/#{ERB::Util.url_encode(recipe.name)}"
      end

      it 'returns the recipe data in the expected schema' do
        expect(response).to be_successful
        assert_response_schema
      end
    end
  end

  context "when the given recipe name doesn't exist" do
    before do
      get "/api/recipes/#{ERB::Util.url_encode(recipe.name)}0"
    end

    it 'returns a Not Found repsonse' do
      expect(response).to have_http_status(:not_found)
    end
  end
end
