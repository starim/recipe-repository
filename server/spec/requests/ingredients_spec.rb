require 'rails_helper'

RSpec.describe "Ingredients controller", type: :request do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  describe 'GET /api/ingredients' do
    context 'when not requesting any special behavior' do
      context 'when there are no ingredients' do
        it 'returns an empty list' do
          get "/api/ingredients"
          expect(response).to be_successful
          expect(json.length).to eq(0)
          assert_response_schema
        end
      end

      context 'when there are ingredients' do
        before do
          create_list :ingredient, 2
        end

        it 'returns a list of all ingredients' do
          get "/api/ingredients"
          expect(response).to be_successful
          expect(json.length).to eq(2)
          assert_response_schema
        end

        it "doesn't include a num_recipes property on the serialized models" do
          get "/api/ingredients"
          expect(response).to be_successful
          expect(json.length).to eq(2)
          expect(json[0].keys).not_to include("num_recipes")
          expect(json[1].keys).not_to include("num_recipes")
          assert_response_schema
        end
      end
    end

    context 'when requesting only the most used ingredients' do
      context 'when there are no ingredients' do
        it 'returns an empty list' do
          get "/api/ingredients", params: { most_used_count: 5 }
          expect(response).to be_successful
          expect(json.length).to eq(0)
          assert_response_schema
        end
      end

      context 'when there are ingredients' do
        before do
          egg = create :ingredient, name: "egg"
          flour = create :ingredient, name: "flour"

          recipe = build :recipe
          recipe.ingredient_preparations <<
            IngredientPreparation.new(recipe: recipe, ingredient: egg)
          recipe.save!

          recipe = build :recipe
          recipe.ingredient_preparations <<
            IngredientPreparation.new(recipe: recipe, ingredient: flour)
          recipe.save!

          recipe = build :recipe
          recipe.ingredient_preparations <<
            IngredientPreparation.new(recipe: recipe, ingredient: flour)
          recipe.save!
        end

        it 'returns a list of the most used ingredients in order from most to least used' do
          get "/api/ingredients", params: { most_used_count: 5 }
          expect(response).to be_successful
          expect(json.length).to eq(2)
          expect(json[0]["name"]).to eq("flour")
          expect(json[1]["name"]).to eq("egg")
          assert_response_schema
        end

        it 'returns the number of recipes each ingredient is used in' do
          get "/api/ingredients", params: { most_used_count: 5 }
          expect(response).to be_successful
          expect(json.length).to eq(2)
          expect(json[0]["num_recipes"]).to eq(2)
          expect(json[1]["num_recipes"]).to eq(1)
          assert_response_schema
        end
      end
    end
  end
end
