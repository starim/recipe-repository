require 'rails_helper'
require 'erb'

RSpec.describe "DELETE /api/recipes/:name", type: :request do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  let!(:recipe) { create :recipe }

  context 'when the given recipe name exists' do
    before do
      delete "/api/recipes/#{ERB::Util.url_encode(recipe.name)}"
    end

    it "returns a successful response" do
      expect(response).to have_http_status(:no_content)
    end

    it 'removes the Recipe model from the database' do
      expect(Recipe.count).to eq(0)
    end
  end

  context "when the given name doesn't exist" do
    before do
      put "/api/recipes/#{ERB::Util.url_encode(recipe.name)}0"
    end

    it "returns a Not Found response" do
      expect(response).to have_http_status(:not_found)
    end
  end
end
