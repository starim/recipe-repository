require 'rails_helper'
require 'erb'

RSpec.describe "POST /api/recipe", type: :request do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  context 'when the input is valid' do

    context 'and the optional parameters are supplied' do

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            description: "A filling breakfast.",
            source: "Trial and error.",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                preparation: "ready",
                amount: 0.5,
                unit: "oz",
              },
            ],
            taggings: [
              {
                tag: "breakfast",
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            comments: "Takes about a half hour to make.",
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.name).to eq("Breakfast Burritos")
        expect(new_model.ingredient_preparations.length).to eq(1)
        expect(new_model.ingredient_preparations.first.preparation).to eq("ready")
        expect(new_model.ingredient_preparations.first.amount).to eq(0.5)
        expect(new_model.ingredient_preparations.first.unit.name).to eq("oz")
        expect(new_model.taggings.count).to eq(1)
        expect(new_model.taggings.first.tag.name).to eq("breakfast")
        expect(new_model.directions).to eq([
          "Buy the ingredients.",
          "Cook the ingredients.",
        ])
        expect(new_model.category.name).to eq("Breakfast")
      end

      it 'sets the optional properties to the given values' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.description).to eq("A filling breakfast.")
        expect(new_model.source).to eq("Trial and error.")
        expect(new_model.comments).to eq("Takes about a half hour to make.")
      end
    end

    context 'and the optional parameters are missing' do

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                amount: 0.5,
              },
            ],
            taggings: [
              {
                tag: "breakfast",
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.name).to eq("Breakfast Burritos")
        expect(new_model.ingredient_preparations.length).to eq(1)
        expect(new_model.ingredient_preparations.first.preparation).to be_nil
        expect(new_model.ingredient_preparations.first.amount).to eq(0.5)
        expect(new_model.ingredient_preparations.first.unit_id).to be_nil
        expect(new_model.taggings.count).to eq(1)
        expect(new_model.taggings.first.tag.name).to eq("breakfast")
        expect(new_model.directions).to eq([
          "Buy the ingredients.",
          "Cook the ingredients.",
        ])
        expect(new_model.category.name).to eq("Breakfast")
      end

      it 'sets default values for the optional properties' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.description).to eq("")
        expect(new_model.source).to be_nil
        expect(new_model.comments).to eq("")
        expect(new_model.ingredient_preparations.length).to eq(1)
        expect(new_model.ingredient_preparations.first.preparation).to be_nil
        expect(new_model.ingredient_preparations.first.unit).to be_nil
      end
    end

    context 'and the optional parameters are present with empty string values' do

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            description: "",
            source: "",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                preparation: "",
                amount: 0.5,
                unit: "",
              },
            ],
            taggings: [
              {
                tag: "breakfast",
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            comments: "",
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.name).to eq("Breakfast Burritos")
        expect(new_model.ingredient_preparations.length).to eq(1)
        expect(new_model.ingredient_preparations.first.preparation).to be_nil
        expect(new_model.ingredient_preparations.first.amount).to eq(0.5)
        expect(new_model.ingredient_preparations.first.unit_id).to be_nil
        expect(new_model.taggings.count).to eq(1)
        expect(new_model.taggings.first.tag.name).to eq("breakfast")
        expect(new_model.directions).to eq([
          "Buy the ingredients.",
          "Cook the ingredients.",
        ])
        expect(new_model.category.name).to eq("Breakfast")
      end

      it 'sets default values for the optional properties' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.description).to eq("")
        expect(new_model.source).to be_nil
        expect(new_model.comments).to eq("")
        expect(new_model.ingredient_preparations.length).to eq(1)
        expect(new_model.ingredient_preparations.first.preparation).to be_nil
        expect(new_model.ingredient_preparations.first.unit).to be_nil
      end
    end

    context 'and directions field has blank entries' do

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                amount: 0.5,
              },
            ],
            directions: [
              "Buy the ingredients.",
              "",
              "Cook the ingredients.",
            ],
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'removes blank directions' do
        expect(Recipe.count).to eq(1)
        new_model = Recipe.first
        expect(new_model.directions.length).to eq(2)
      end
    end

    context 'and the form specifies a category that already exists' do

      let!(:category) { create :category }

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            description: "A filling breakfast.",
            source: "Trial and error.",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                preparation: "ready",
                amount: 0.5,
                unit: "oz",
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            comments: "Takes about a half hour to make.",
            category: category.name,
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
      end

      it 'associates the new recipe to the existing category instead of creating a duplicate category' do
        new_model = Recipe.first
        expect(new_model.category_id).to eq(category.id)
      end
    end

    context 'and the form specifies an ingredient that already exists' do

      let!(:ingredient) { create :ingredient }

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            description: "A filling breakfast.",
            source: "Trial and error.",
            ingredient_preparations: [
              {
                ingredient: ingredient.name,
                preparation: "ready",
                amount: 0.5,
                unit: "oz",
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            comments: "Takes about a half hour to make.",
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
      end

      it 'associates the new recipe to the existing ingredient instead of creating a duplicate ingredient' do
        new_model = Recipe.first
        new_recipe_ingredient_id =
          new_model.ingredient_preparations.first.ingredient_id
        expect(new_recipe_ingredient_id).to eq(ingredient.id)
      end
    end

    context 'and the form specifies a unit that already exists' do

      let!(:unit) { create :unit }

      before do
        params = {
          recipe: {
            name: "Breakfast Burritos",
            description: "A filling breakfast.",
            source: "Trial and error.",
            ingredient_preparations: [
              {
                ingredient: "Egg",
                preparation: "ready",
                amount: 0.5,
                unit: unit.name,
              },
            ],
            directions: [
              "Buy the ingredients.",
              "Cook the ingredients.",
            ],
            comments: "Takes about a half hour to make.",
            category: "Breakfast",
          }
        }

        post '/api/recipes', params: params.to_json, headers: headers
      end

      it "returns a Created response" do
        expect(response).to have_http_status(:created)
      end

      it 'saves a new Recipe model in the database' do
        expect(Recipe.count).to eq(1)
      end

      it 'associates the new recipe to the existing unit instead of creating a duplicate unit' do
        new_model = Recipe.first
        new_recipe_unit_id =
          new_model.ingredient_preparations.first.unit_id
        expect(new_recipe_unit_id).to eq(unit.id)
      end
    end
  end

  context 'when required parameters are missing' do
    before do
      params = { recipe: {} }

      post '/api/recipes', params: params.to_json, headers: headers
    end

    it 'returns a Bad Request response with an error message' do
      expect(response).to have_http_status(:bad_request)
      expect(json.keys).to include("errors")
    end

    it "doesn't save a new Recipe model" do
      expect(Recipe.count).to eq(0)
    end

    it "doesn't save a new IngredientPreparation model" do
      expect(IngredientPreparation.count).to eq(0)
    end
  end

  context 'when the new ingredient preparation data is blank' do
    before do
      params = {
        recipe: {
          name: "Breakfast Burritos",
          ingredient_preparations: [
            {
              ingredient: "",
              preparation: "",
              amount: nil,
              unit: "",
            },
          ],
          directions: [
            "Buy the ingredients.",
            "Cook the ingredients.",
          ],
          category: "Breakfast",
        }
      }

      post '/api/recipes', params: params.to_json, headers: headers
    end

    it "returns a Created response" do
      expect(response).to have_http_status(:created)
    end

    it "saves a new Recipe model" do
      expect(Recipe.count).to eq(1)
    end

    it "ignores the blank ingredient preparation" do
      expect(IngredientPreparation.count).to eq(0)
    end
  end

  context 'when an ingredient preparation is invalid' do
    before do
      params = {
        recipe: {
          name: "Breakfast Burritos",
          ingredient_preparations: [
            {
              ingredient: "",
              preparation: "",
              amount: -1.0,
              unit: "",
            },
],
          directions: [
            "Buy the ingredients.",
            "Cook the ingredients.",
          ],
          category: "Breakfast",
        }
      }

      post '/api/recipes', params: params.to_json, headers: headers
    end

    it 'returns a Bad Request response with the error message' do
      expect(response).to have_http_status(:bad_request)
      expect(json.keys).to include("errors")
    end

    it "doesn't save a new Recipe model" do
      expect(Recipe.count).to eq(0)
    end

    it "doesn't save a new IngredientPreparation model" do
      expect(IngredientPreparation.count).to eq(0)
    end
  end
end
