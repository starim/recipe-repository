require 'rails_helper'

RSpec.describe "Units controller", type: :request do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  describe 'GET /api/units' do
    context 'when there are no units' do
      it 'returns an empty list' do
        get "/api/units"
        expect(response).to be_successful
        expect(json.length).to eq(0)
        assert_response_schema
      end
    end

    context 'when there are units' do
      before do
        create_list :unit, 2
      end

      it 'returns a list of all units' do
        get "/api/units"
        expect(response).to be_successful
        expect(json.length).to eq(2)
        assert_response_schema
      end
    end
  end
end
