require 'rails_helper'
require 'erb'

RSpec.describe "PUT/PATCH /api/recipes/:name", type: :request do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  let!(:beef_fire) { create :ingredient, name: "Beef Fire" }
  let!(:celery) { create :ingredient, name: "Celery" }
  let!(:recipe) do
    recipe = create :recipe, name: "Spicy Beef", ingredients_count: 0
    create :ingredient_preparation,
      recipe: recipe, ingredient: beef_fire
    create :ingredient_preparation,
      recipe: recipe, ingredient: celery
    recipe.reload
    recipe
  end

  context 'when the input is valid' do
    before do
      params = {
        recipe: {
          name: "Biscuits and Gravy",
          description: "A hearty breakfast.",
          source: "Practice.",
          ingredient_preparations: [
            {
              ingredient: "Eggs",
              preparation: "chopped",
              amount: 2.5,
              unit: "oz",
            },
            {
              ingredient: "Beef Fire",
              preparation: "sauce",
              amount: "2.5",
              unit: "oz",
            },
          ],
          directions: [
            "Buy.",
            "Cook.",
          ],
          taggings: [
            {
              tag: "breakfast",
            },
          ],
          comments: "Takes about a half hour to make.",
          category: "Breakfast",
        }
      }

      put(
        "/api/recipes/#{ERB::Util.url_encode(recipe.name)}",
        params: params.to_json, headers: headers
      )
    end

    it "returns a successful response" do
      expect(response).to have_http_status(:no_content)
    end

    it 'updates the Recipe model in the database' do
      expect(Recipe.count).to eq(1)
      model = Recipe.first
      expect(model.name).to eq("Biscuits and Gravy")
      expect(model.taggings.length).to eq(1)
      expect(model.taggings.first.tag.name).to eq("breakfast")
      expect(model.directions).to eq([
        "Buy.",
        "Cook.",
      ])
      expect(model.category.name).to eq("Breakfast")
    end

    it 'updates the IngredientPreparation model in the database' do
      expect(Recipe.count).to eq(1)
      model = Recipe.first

      beef_fire = model.ingredient_preparations.find do |ingredient_preparation|
        ingredient_preparation.ingredient.name == "Beef Fire"
      end
      expect(beef_fire).not_to be_nil
      expect(beef_fire.preparation).to eq("sauce")
      expect(beef_fire.ingredient.name).to eq("Beef Fire")
      expect(beef_fire.amount).to eq(2.5)
      expect(beef_fire.unit.name).to eq("oz")
    end

    it 'adds the new IngredientPreparation model to the database' do
      expect(Recipe.count).to eq(1)
      model = Recipe.first

      eggs = model.ingredient_preparations.find do |ingredient_preparation|
        ingredient_preparation.ingredient.name == "Eggs"
      end
      expect(eggs).not_to be_nil
      expect(eggs.preparation).to eq("chopped")
      expect(eggs.amount).to eq(2.5)
      expect(eggs.ingredient.name).to eq("Eggs")
      expect(eggs.unit.name).to eq("oz")
    end

    it 'removes the unused IngredientPreparation model from the database' do
      expect(Recipe.count).to eq(1)
      model = Recipe.first
      expect(model.ingredient_preparations.length).to eq(2)

      celery = model.ingredient_preparations.find do |ingredient_preparation|
        ingredient_preparation.ingredient.name == "Celery"
      end
      expect(celery).to be_nil
    end
  end

  context 'when the new ingredient preparation data is blank' do
    before do
      params = {
        recipe: {
          ingredient_preparations: [
            { ingredient: "Beef Fire" },
            { ingredient: "Celery" },
            {
              ingredient: "",
              preparation: "",
              amount: nil,
              unit: "",
            },
          ],
        }
      }

      patch(
        "/api/recipes/#{ERB::Util.url_encode(recipe.name)}",
        params: params.to_json, headers: headers
      )
    end

    it 'ignores the ingredient preparation' do
      expect(response).to have_http_status(:no_content)

      expect(Recipe.count).to eq(1)
      model = Recipe.first
      expect(model.ingredient_preparations.length).to eq(2)
      ingredient_names = model.ingredient_preparations.map do |prep|
        prep.ingredient.name
      end
      expect(ingredient_names).to match_array(["Beef Fire", "Celery"])
    end
  end

  context 'when an ingredient preparation is invalid' do
    before do
      params = {
        recipe: {
          ingredient_preparations: [
            {
              ingredient: "",
              preparation: "",
              amount: -1.0,
              unit: "",
            },
          ],
        }
      }

      patch "/api/recipes/#{ERB::Util.url_encode(recipe.name)}", params: params.to_json, headers: headers
    end

    it 'returns a Bad Request response with an error message' do
      expect(response).to have_http_status(:bad_request)
      expect(json.keys).to include("errors")
    end
  end

  context 'when a tagging is invalid' do
    before do
      params = {
        recipe: {
          taggings: [{}],
        }
      }

      patch "/api/recipes/#{ERB::Util.url_encode(recipe.name)}", params: params.to_json, headers: headers
    end

    it 'returns a Bad Request response with an error message' do
      expect(response).to have_http_status(:bad_request)
      expect(json.keys).to include("errors")
    end
  end

  context 'when you try to apply the same tag twice' do
    before do
      params = {
        recipe: {
          taggings: [
            {
              tag: "breakfast"
            }
          ],
        }
      }

      patch "/api/recipes/#{ERB::Util.url_encode(recipe.name)}", params: params.to_json, headers: headers
    end

    it 'it allows the update, but does not add a new tag' do
      expect(response).to have_http_status(:no_content)
      expect(Recipe.count).to eq(1)
      model = Recipe.first
      expect(model.taggings.count).to eq(1)
      expect(model.taggings.first.tag.name).to eq("breakfast")
    end
  end

  context "when the given name doesn't exist" do
    before do
      params = {
        recipe: {},
      }

      put "/api/recipes/#{ERB::Util.url_encode(recipe.name)}0", params: params.to_json, headers: headers
    end

    it "returns a Not Found response" do
      expect(response).to have_http_status(:not_found)
    end
  end

  context "when an id parameter is passed" do
    before do
      params = {
        recipe: {
          id: -1,
          description: "New Description",
        }
      }

      put "/api/recipes/#{ERB::Util.url_encode(recipe.name)}", params: params.to_json, headers: headers
    end

    it 'ignores the id parameter' do
      expect(response).to have_http_status(:no_content)
      expect(recipe.id).not_to eq(-1)
    end
  end
end
