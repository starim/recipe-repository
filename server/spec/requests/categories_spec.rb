require 'rails_helper'

RSpec.describe "Categories controller", type: :request do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  describe 'GET /api/categories' do
    context 'when there are no categories' do
      it 'returns an empty list' do
        get "/api/categories"
        expect(response).to be_successful
        expect(json.length).to eq(0)
        assert_response_schema
      end
    end

    context 'when there are categories' do
      before do
        create_list :category, 2
      end

      it 'returns a list of all categories' do
        get "/api/categories"
        expect(response).to be_successful
        expect(json.length).to eq(2)
        assert_response_schema
      end
    end
  end
end
