require 'rails_helper'

RSpec.describe "Tags controller", type: :request do

  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  describe 'GET /api/tags' do
    context 'when there are no tags' do
      it 'returns an empty list' do
        get "/api/tags"
        expect(response).to be_successful
        expect(json.length).to eq(0)
        assert_response_schema
      end
    end

    context 'when there are tags' do
      before do
        create_list :tag, 2
      end

      it 'returns a list of all tags' do
        get "/api/tags"
        expect(response).to be_successful
        expect(json.length).to eq(2)
        assert_response_schema
      end
    end
  end

  describe 'POST /api/tags' do
    context 'when there are no tags' do
      before do
        params = {
          tag: {
            name: "breakfast",
          }
        }
        post '/api/tags', params: params.to_json, headers: headers
      end

      it 'saved one element' do
        expect(Tag.count).to eq(1)
        new_model = Tag.first
        expect(new_model.name).to eq("breakfast")
      end
    end

    context 'will not add a new model when name already exists' do
      before do
        params = {
          tag: {
            name: "breakfast",
          }
        }
        post '/api/tags', params: params.to_json, headers: headers
        post '/api/tags', params: params.to_json, headers: headers
      end

      it 'returns a Bad Request response with an error message' do
        expect(response).to have_http_status(:bad_request)
        expect(json.keys).to include("errors")
      end

      it 'saved only one element' do
        expect(Tag.count).to eq(1)
        new_model = Tag.first
        expect(new_model.name).to eq("breakfast")
      end
    end
  end

  describe 'DELETE /api/tags/{id}' do
    context 'will delete when id is correct' do
      before do
        params = {
          tag: {
            name: "breakfast",
          }
        }
        post '/api/tags', params: params.to_json, headers: headers
      end

      it 'deletes the saved model' do
        expect(Tag.count).to eq(1)
        new_model = Tag.first
        expect(new_model.name).to eq("breakfast")
        delete '/api/tags/breakfast', headers: headers
        expect(Tag.count).to eq(0)
      end
    end

    context 'wont delete when id is not correct' do
      before do
        params = {
          tag: {
            name: "breakfast",
          }
        }
        post '/api/tags', params: params.to_json, headers: headers
      end

      it 'returns 404' do
        expect(Tag.count).to eq(1)
        new_model = Tag.first
        expect(new_model.name).to eq("breakfast")
        delete '/api/tags/Dinner', headers: headers
        expect(Tag.count).to eq(1)
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
