require 'rails_helper'
require 'erb'

RSpec.describe "GET /api/recipes", type: :request do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  context 'when there are no recipes' do
    it 'returns an empty list' do
      get "/api/recipes"
      expect(response).to be_successful
      expect(json.length).to eq(0)
      assert_response_schema
    end
  end

  context 'when there are recipes' do
    context 'and no filters are passed' do
      before do
        # have at least one recipe with a unit set on its ingredient
        # preparations in order to test that association gets serialized
        recipe = create :recipe
        recipe.ingredient_preparations.first.unit = create :unit
        recipe.save!

        create :recipe
      end

      it 'returns a list of all recipes' do
        get "/api/recipes"
        expect(response).to be_successful
        expect(json.length).to eq(2)
        assert_response_schema
      end
    end

    context 'and filters are passed' do
      before do
        breakfast = create :category, name: "Breakfast"
        dinner = create :category, name: "Dinner"

        egg = create :ingredient, name: "Egg"
        flour = create :ingredient, name: "Flour"
        tortilla = create :ingredient, name: "Tortilla"
        sausage = create :ingredient, name: "Sausage"

        favorites = create :tag, name: "My Favorites"

        recipe1 = create(
          :recipe,
          name: "Brent's Eggs & Gravy",
          category: breakfast,
          ingredients_count: 0,
          tags_count: 0,
        )
        recipe2 = create(
          :recipe,
          name: "Devin's Pancakes",
          category: breakfast,
          ingredients_count: 0,
          tags_count: 0,
        )
        recipe3 = create(
          :recipe,
          name: "Brent's Tacos",
          category: dinner,
          ingredients_count: 0,
          tags_count: 0,
        )
        recipe4 = create(
          :recipe,
          name: "Biscuits & Gravy",
          category: breakfast,
          ingredients_count: 0,
          tags_count: 0,
        )

        create :ingredient_preparation, recipe: recipe1, ingredient: egg
        create :ingredient_preparation, recipe: recipe2, ingredient: egg
        create :ingredient_preparation, recipe: recipe1, ingredient: flour
        create :ingredient_preparation, recipe: recipe2, ingredient: flour
        create :ingredient_preparation, recipe: recipe4, ingredient: flour
        create :ingredient_preparation, recipe: recipe3, ingredient: tortilla
        create :ingredient_preparation, recipe: recipe4, ingredient: sausage

        create :tagging, recipe: recipe1, tag: favorites
        create :tagging, recipe: recipe2, tag: favorites
      end

      context 'for recipe name' do
        it 'returns a filtered list of recipes' do
          get "/api/recipes", params: { recipe_name_filter: "Devin" }
          expect(response).to be_successful
          matches = json.map {|recipe| recipe["name"]}
          expect(matches).to match_array(["Devin's Pancakes"])
          assert_response_schema
        end
      end

      context 'for category name' do
        it 'returns a filtered list of recipes' do
          get "/api/recipes", params: { category_filter: "breakfast" }
          expect(response).to be_successful
          matches = json.map {|recipe| recipe["name"]}
          expect(matches).to match_array(
            ["Brent's Eggs & Gravy", "Devin's Pancakes", "Biscuits & Gravy"])
          assert_response_schema
        end
      end

      context 'for ingredient name' do
        it 'returns a filtered list of recipes' do
          get "/api/recipes", params: { ingredient_filter: "a" }
          expect(response).to be_successful
          matches = json.map {|recipe| recipe["name"]}
          expect(matches).to match_array(
            ["Brent's Tacos", "Biscuits & Gravy"])
          assert_response_schema
        end
      end

      context 'for tag name' do
        it 'returns a filtered list of recipes' do
          get "/api/recipes", params: { tag_filter: "fav" }
          expect(response).to be_successful
          matches = json.map {|recipe| recipe["name"]}
          expect(matches).to match_array(
            ["Brent's Eggs & Gravy", "Devin's Pancakes"])
          assert_response_schema
        end
      end

      context 'for multiple filters' do
        it 'returns a filtered list of recipes' do
          get "/api/recipes", params: {
            ingredient_filter: "egg", recipe_name_filter: "Devin"
          }
          expect(response).to be_successful
          expect(json.length).to eq(1)
          assert_response_schema
        end
      end
    end
  end
end
