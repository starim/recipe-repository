# == Schema Information
#
# Table name: recipes
#
#  id          :bigint(8)        not null, primary key
#  name        :text             not null
#  description :text             default(""), not null
#  source      :text
#  directions  :text             not null, is an Array
#  comments    :text             default(""), not null
#  category_id :bigint(8)        not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it { should validate_presence_of(:name) }
  it { should allow_value("").for(:description) }
  it { should allow_value(nil).for(:description) }
  it { should_not allow_value([]).for(:directions) }
  it { should_not allow_value(nil).for(:directions) }

  describe "uniqueness validations" do
    subject { create :recipe }
    it { should validate_uniqueness_of(:name) }
  end

  it { should have_many(:ingredient_preparations).inverse_of(:recipe) }
  it { should have_many(:ingredients)
        .through(:ingredient_preparations)
        .inverse_of(:recipes) }

  it { should have_many(:taggings).inverse_of(:recipe) }
  it { should have_many(:tags)
        .through(:taggings)
        .inverse_of(:recipes) }

  it "deletes associated IngredientPreparations from the database when destroyed" do
    recipe = create :recipe, ingredients_count: 2
    associated_ingredient_prep_ids = recipe.ingredient_preparations.map do |model|
      model.id
    end
    recipe.destroy!
    associated_ingredient_prep_ids.each do |id|
      expect(IngredientPreparation.where(id: id).count).to eq(0)
    end
  end

  it "deletes associated Taggings from the database when destroyed" do
    recipe = create :recipe, tags_count: 2
    associated_tagging_ids = recipe.taggings.map do |model|
      model.id
    end
    recipe.destroy!
    associated_tagging_ids.each do |id|
      expect(Tagging.where(id: id).count).to eq(0)
    end
  end

  describe "search methods" do

    before do
      breakfast = create :category, name: "Breakfast"
      dinner = create :category, name: "Dinner"

      egg = create :ingredient, name: "Egg"
      egg_white = create :ingredient, name: "Egg White"
      flour = create :ingredient, name: "Flour"
      tortilla = create :ingredient, name: "Tortilla"
      sausage = create :ingredient, name: "Sausage"

      favorites = create :tag, name: "My Favorites"

      recipe1 = create(
        :recipe,
        name: "Brent's Eggs & Gravy",
        category: breakfast,
        ingredients_count: 0,
        tags_count: 0,
      )
      recipe2 = create(
        :recipe,
        name: "Devin's Pancakes",
        category: breakfast,
        ingredients_count: 0,
        tags_count: 0,
      )
      recipe3 = create(
        :recipe,
        name: "Brent's Tacos",
        category: dinner,
        ingredients_count: 0,
        tags_count: 0,
      )
      recipe4 = create(
        :recipe,
        name: "Biscuits & Gravy",
        category: breakfast,
        ingredients_count: 0,
        tags_count: 0,
      )

      create :ingredient_preparation, recipe: recipe1, ingredient: egg
      create :ingredient_preparation, recipe: recipe1, ingredient: egg_white
      create :ingredient_preparation, recipe: recipe2, ingredient: egg
      create :ingredient_preparation, recipe: recipe1, ingredient: flour
      create :ingredient_preparation, recipe: recipe2, ingredient: flour
      create :ingredient_preparation, recipe: recipe4, ingredient: flour
      create :ingredient_preparation, recipe: recipe3, ingredient: tortilla
      create :ingredient_preparation, recipe: recipe4, ingredient: sausage

      create :tagging, recipe: recipe1, tag: favorites
      create :tagging, recipe: recipe2, tag: favorites
    end

    describe "based on name" do
      it "fuzzy matches the specified recipe name" do
        matches = Recipe.name_like("rent").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy", "Brent's Tacos"]
        )
      end

      it "doesn't filter if the specified name is nil" do
        matches = Recipe.name_like(nil).map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end

      it "doesn't filter if the specified name is blank" do
        matches = Recipe.name_like("").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end
    end

    describe "based on category name" do
      it "fuzzy matches the specified category name" do
        matches = Recipe.category_name_like("din").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Tacos"]
        )
      end

      it "doesn't filter if the specified name is nil" do
        matches = Recipe.category_name_like(nil).map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end

      it "doesn't filter if the specified name is blank" do
        matches = Recipe.category_name_like("").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end
    end

    describe "based on ingredient name" do
      it "fuzzy matches the specified ingredient name" do
        matches = Recipe.ingredient_name_like("our").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy", "Devin's Pancakes", "Biscuits & Gravy"]
        )
      end

      it "doesn't filter if the specified name is nil" do
        matches = Recipe.ingredient_name_like(nil).map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end

      it "doesn't filter if the specified name is blank" do
        matches = Recipe.ingredient_name_like("").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end

      it "doesn't return multiple instances of the same recipe if it has multiple ingredients that match the filter" do
        matches = Recipe.ingredient_name_like("eg").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy", "Devin's Pancakes"]
        )
      end
    end

    describe "based on tag name" do
      it "fuzzy matches the specified tag name" do
        matches = Recipe.tag_name_like("fav").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy", "Devin's Pancakes"]
        )
      end

      it "doesn't filter if the specified name is nil" do
        matches = Recipe.tag_name_like(nil).map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end

      it "doesn't filter if the specified name is blank" do
        matches = Recipe.tag_name_like("").map {|recipe| recipe.name }
        expect(matches).to match_array(
          ["Brent's Eggs & Gravy",
           "Devin's Pancakes",
           "Brent's Tacos",
           "Biscuits & Gravy"]
        )
      end
    end

    it "can be chained" do
      matches = Recipe
        .name_like("brent")
        .category_name_like("breakfast")
        .ingredient_name_like("flour")
        .tag_name_like("fav")
        .map {|recipe| recipe.name }
      expect(matches).to match_array(["Brent's Eggs & Gravy"])
    end
  end
end
