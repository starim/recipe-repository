# == Schema Information
#
# Table name: ingredients
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  it { should validate_presence_of(:name) }

  describe "uniqueness validations" do
    subject { create :ingredient }
    it { should validate_uniqueness_of(:name) }
  end

  it { should have_many(:ingredient_preparations).inverse_of(:ingredient) }
  it { should have_many(:recipes)
        .through(:ingredient_preparations)
        .inverse_of(:ingredients) }

  describe "most_used" do
    context "when there are fewer ingredients than requested" do

      let!(:egg) { create :ingredient, name: "egg" }
      let!(:flour) { create :ingredient, name: "flour" }
      let!(:fermented_tofu) { create :ingredient, name: "fermented_tofu" }

      before do
        recipe = build :recipe
        recipe.ingredient_preparations <<
          IngredientPreparation.new(recipe: recipe, ingredient: egg)
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: flour),
        ]
        recipe.save!
      end

      it "returns all ingredients that have been used at least once in order of most used" do
        ingredients = Ingredient.most_used(5)
        expect(ingredients.length).to eq(2)
        expect(ingredients.first).to eq(egg)
        expect(ingredients.last).to eq(flour)
      end

      it "sets the number of associated recipes on each returned Ingredient model" do
        ingredients = Ingredient.most_used(5)
        expect(ingredients.length).to eq(2)
        expect(ingredients.first.num_recipes).to eq(2)
        expect(ingredients.last.num_recipes).to eq(1)
      end
    end

    context "when there are more ingredients than requested" do
      let!(:egg) { create :ingredient, name: "egg" }
      let!(:flour) { create :ingredient, name: "flour" }
      let!(:rice) { create :ingredient, name: "rice" }
      let!(:tortilla) { create :ingredient, name: "tortilla" }
      let!(:cheddar_cheese) { create :ingredient, name: "cheddar_cheese" }
      let!(:bell_pepper) { create :ingredient, name: "bell_pepper" }
      let!(:cilantro) { create :ingredient, name: "cilantro" }
      let!(:avocado) { create :ingredient, name: "avocado" }
      let!(:serrano_pepper) { create :ingredient, name: "serrano pepper" }
      let!(:fermented_tofu) { create :ingredient, name: "fermented tofu" }

      before do
        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: bell_pepper),
          IngredientPreparation.new(recipe: recipe, ingredient: cheddar_cheese),
          IngredientPreparation.new(recipe: recipe, ingredient: tortilla),
          IngredientPreparation.new(recipe: recipe, ingredient: rice),
        ]
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: cheddar_cheese),
          IngredientPreparation.new(recipe: recipe, ingredient: serrano_pepper),
        ]
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: avocado),
        ]
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: cheddar_cheese),
          IngredientPreparation.new(recipe: recipe, ingredient: bell_pepper),
        ]
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: cheddar_cheese),
          IngredientPreparation.new(recipe: recipe, ingredient: bell_pepper),
          IngredientPreparation.new(recipe: recipe, ingredient: rice),
        ]
        recipe.save!
      end

      it "returns the specified number of ingredients in order of most used" do
        ingredients = Ingredient.most_used(4)
        expect(ingredients.length).to eq(4)
        expect(ingredients[0]).to eq(egg)
        expect(ingredients[1]).to eq(cheddar_cheese)
        expect(ingredients[2]).to eq(bell_pepper)
        expect(ingredients[3]).to eq(rice)
      end

      it "sets the number of associated recipes on each returned Ingredient model" do
        ingredients = Ingredient.most_used(4)
        expect(ingredients.length).to eq(4)
        expect(ingredients[0].num_recipes).to eq(5)
        expect(ingredients[1].num_recipes).to eq(4)
        expect(ingredients[2].num_recipes).to eq(3)
        expect(ingredients[3].num_recipes).to eq(2)
      end
    end

    context "when an ingredient is attached to a recipe multiple times" do

      let!(:egg) { create :ingredient, name: "egg" }
      let!(:flour) { create :ingredient, name: "flour" }

      before do
        recipe = build :recipe
        recipe.ingredient_preparations << [
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
          IngredientPreparation.new(recipe: recipe, ingredient: egg),
        ]
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations <<
          IngredientPreparation.new(recipe: recipe, ingredient: flour)
        recipe.save!

        recipe = build :recipe
        recipe.ingredient_preparations <<
          IngredientPreparation.new(recipe: recipe, ingredient: flour)
        recipe.save!
      end

      it "only counts the usage once" do
        ingredients = Ingredient.most_used(1)
        expect(ingredients.length).to eq(1)
        expect(ingredients.first).to eq(flour)
      end

      it "sets the number of associated recipes on each returned Ingredient model" do
        ingredients = Ingredient.most_used(1)
        expect(ingredients.length).to eq(1)
        expect(ingredients[0].num_recipes).to eq(2)
      end
    end
  end
end
