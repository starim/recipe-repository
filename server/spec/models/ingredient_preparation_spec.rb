# == Schema Information
#
# Table name: ingredient_preparations
#
#  id            :bigint(8)        not null, primary key
#  preparation   :text
#  amount        :decimal(, )
#  recipe_id     :bigint(8)
#  ingredient_id :bigint(8)
#  unit_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe IngredientPreparation, type: :model do
  it { should belong_to(:recipe) }
  it { should belong_to(:ingredient) }
  it { should belong_to(:unit).optional }
end
