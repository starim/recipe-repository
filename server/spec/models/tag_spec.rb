# == Schema Information
#
# Table name: tags
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Tag, type: :model do
  it { should validate_presence_of(:name) }

  describe "uniqueness validations" do
    subject { create :tag }
    it { should validate_uniqueness_of(:name).case_insensitive }
  end

  it { should have_many(:taggings) }
  it { should have_many(:recipes)
        .through(:taggings)
        .inverse_of(:tags) }

  it "normalizes all tag names to lower case" do
    tag = create :tag, name: "KuKuCachoo!"
    tag.reload
    expect(tag.name).to eq("kukucachoo!")
  end

end
