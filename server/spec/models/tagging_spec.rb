# == Schema Information
#
# Table name: taggings
#
#  id         :bigint(8)        not null, primary key
#  recipe_id  :bigint(8)        not null
#  tag_id     :bigint(8)        not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Tagging, type: :model do
  it { should belong_to(:recipe) }
  it { should belong_to(:tag) }

  describe "validations" do
    subject { create :tagging }
    # https://github.com/thoughtbot/shoulda-matchers/issues/814
    it { should validate_uniqueness_of(:recipe).scoped_to(:tag_id).case_insensitive }
  end

end
