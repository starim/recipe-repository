# == Schema Information
#
# Table name: categories
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  it { should validate_presence_of(:name) }

  describe "uniqueness validations" do
    subject { create :category }
    it { should validate_uniqueness_of(:name) }
  end

  it { should have_many(:recipes).inverse_of(:category) }
  it { should belong_to(:parent)
        .optional
        .class_name("Category")
        .with_foreign_key(:parent_id) }
  it { should have_many(:children).class_name("Category").inverse_of(:parent) }
end
