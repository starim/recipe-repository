# == Schema Information
#
# Table name: units
#
#  id         :bigint(8)        not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Unit, type: :model do
  it { should validate_presence_of(:name) }

  describe "uniqueness validations" do
    subject { create :unit }
    it { should validate_uniqueness_of(:name) }
  end

  it { should have_many(:ingredient_preparations).inverse_of(:unit) }
end
