const path = require('path');
const { BundleAnalyzerPlugin }  = require('webpack-bundle-analyzer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = (env) => {

  // Loaders

  // EsLint for both .jsx and .js.
  // 'plugin:react/recommended' must be included as 'extends' in eslintrc.js
  // https://github.com/webpack-contrib/eslint-loader
  const lintingLoaders = {
    enforce: 'pre',
    test: /\.(js|jsx)$/,
    loader: 'eslint-loader',
    exclude: /node_modules/,
  };

  // Allows static assets to be `required()` as part of a bundle and fingerprinted.
  // https://github.com/webpack-contrib/file-loader
  const staticAssetLoaders = {
    test: /\.(png|svg|jpg|gif)$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          esModule: false,
          outputPath: 'assets',
        }
      },
    ],
  };

  // Chain of loaders to handle styling in .scss files.
  // Loaders are chained in reverse order.
  // sass-loader -> convert scss to css.
  // https://github.com/webpack-contrib/sass-loader
  // postcss-loader -> applies postcss tools to the css, such as minification and autoprefixer.
  // https://github.com/postcss/postcss-loader
  // css-loader -> Moves your styling into the bundle, normally the last step.
  // https://www.npmjs.com/package/css-loader
  // MiniCssExtractPlugin.loader -> But I prefer having the styling be in a seaprate file.
  // https://github.com/webpack-contrib/mini-css-extract-plugin
  const styleLoaders = {
    test: /\.(s*)css$/,
    use: [
      MiniCssExtractPlugin.loader,
      'css-loader',
      'postcss-loader',
      'sass-loader',
    ],
  };

  // Transpile all javascript to browser-safe versions with babel.
  // https://github.com/babel/babel-loader
  const traspilingLoaders = {
    test: /\.(mjs|js|jsx)$/,
    exclude: /(node_modules)/,
    use: {
      loader: 'babel-loader',
    },
  };

  // Plugins

  // Analyzes generated bundles and shows their size and contents.
  // https://www.npmjs.com/package/webpack-bundle-analyzer
  const bundleAnalyzerPlugin = new BundleAnalyzerPlugin({
    analyzerMode: 'disabled', /* Comment to activate. */
  });

  // Cleans dist folder on every build.
  // https://www.npmjs.com/package/clean-webpack-plugin
  const cleanWebpackPlugin = new CleanWebpackPlugin();

  // Generates HTML the index.html from a template with all dynamic tags.
  // https://webpack.js.org/plugins/html-webpack-plugin/
  const htmlWebpackPlugin = new HtmlWebpackPlugin({
    filename: 'index.html',
    template: './src/index.html',
  });

  // Extracts the CSS file out of the bundle.js into its own css file to be loaded in parallel.
  // https://github.com/webpack-contrib/mini-css-extract-plugin
  const miniCSSExtractPlugin = new MiniCssExtractPlugin({
    filename: '[name].css',
    chunkFilename: '[id].css',
  });

  // Build Config

  const config = {
    entry: ['./src/index.jsx', './src/styles.scss' ],
    output: {
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
      filename: '[name].[chunkhash].js',
    },
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          node_vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all'
          },
        },
      },
    },
    module: {
      rules: [
        lintingLoaders,
        staticAssetLoaders,
        styleLoaders,
        traspilingLoaders,
      ],
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './assets/'),
        '~': path.resolve(__dirname, './src/'),
      },
      extensions: ['.wasm', '.mjs', '.js', '.json', '.jsx'],
    },
    plugins: [
      bundleAnalyzerPlugin,
      cleanWebpackPlugin,
      htmlWebpackPlugin,
      miniCSSExtractPlugin,
    ],
  };

  // Environment Dependent Config

  const isDevelopment = env === 'development';
  const isProduction = env === 'production';

  if(isDevelopment) {
    config.mode = 'development';
    config.devtool = 'inline-source-map';
    config.devServer = {
      contentBase: [path.join(__dirname, 'src'), path.join(__dirname, 'assets')],
      compress: true,
      historyApiFallback: true,
      open: true,
      overlay: {
        errors: true,
        warnings: true,
      },
      port: 8080,
      watchContentBase: true,
    }
  }

  if(isProduction) {
    config.mode = 'production';
    config.devtool = 'source-map';
  }

  return config;
}
