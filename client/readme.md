Running
--

Install all dependencies with `npm install` then use `npm run serve` to serve
the project locally. Make sure to run `rails s` from the server folder at the
same time using tmux, screen, or a second terminal window.

Testing
--

We use
[react-testing-library](https://github.com/kentcdodds/react-testing-library)
for tests. This library re-exports the DOM query API from
[dom-testing-library](https://github.com/kentcdodds/dom-testing-library).
