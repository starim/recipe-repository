const production = (process.env.NODE_ENV === 'production');

const settings = {
  apiUrl: production ? 'https://recipes-api.vikingshortboat.com:444/api' : 'http://localhost:3000/api',
};

export default settings;
