import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
const parseDecimalNumber = require('parse-decimal-number');

import settings from './../settings';
import Loading from './ui/loading';
import RecipeForm from './recipe_form/recipe_form';
import ErrorBox from './ui/error_box';

const isTest = (process && process.env && process.env.NODE_ENV === "test");

class EditableRecipe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipe: {
        name: "",
        description: "",
        category: "",
        source: "",
        tags: [""],
        ingredient_preparations: [this.defaultIngredientPreparation()],
        directions: [""],
        comments: "",
      },
      lastErrorHtml: null,
      formSubmissionInProgress: false,
      ingredientsList: null,
      unitsList: null,
      categoriesList: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleIngredientPreparationAddition =
      this.handleIngredientPreparationAddition.bind(this);
    this.handleIngredientPreparationChange =
      this.handleIngredientPreparationChange.bind(this);
    this.handleIngredientPreparationRemoval =
      this.handleIngredientPreparationRemoval.bind(this);
  }

  defaultIngredientPreparation() {
    return {
      preparation: "",
      amount: 1.0,
      unit: "",
      ingredient: "",
    };
  }

  fetchRecipeProperties() {
    axios
      .get(`${settings.apiUrl}/recipes/${encodeURIComponent(this.props.name)}`)
      .then(response => {
        const recipe = Object.assign({}, response.data);
        if(recipe.source === null) {
          recipe.source = "";
        }
        if(recipe.category === null) {
          recipe.category = "";
        }
        if(response.data.directions) {
          recipe.directions = response.data.directions.slice();
        }
        recipe.ingredient_preparations =
          recipe.ingredient_preparations.map(prep => {
            const amount = prep.amount ? parseDecimalNumber(prep.amount) : null;
            return {
              unit: prep.unit || "",
              preparation: prep.preparation || "",
              ingredient: prep.ingredient || "",
              amount,
            };
          });
        this.setState({ recipe });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching recipe from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const lastErrorHtml = (
          <p>{`Couldn't fetch recipe data from server: ${errorMessage}`}</p>
        );
        this.setState({ lastErrorHtml });
      });
  }

  fetchResourceList(resourceName) {
    axios
      .get(`${settings.apiUrl}/${resourceName}/`)
      .then(response => {
        const names = response.data.map(object => object.name);
        this.setState({[`${ resourceName}List`]: names });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching resource ${resourceName} from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const lastErrorHtml = (
          <React.Fragment>
            <p>{
              `Error fetching ${resourceName} list from server: ${errorMessage}`
            }</p>
            <p>{
              `Autocomplete won't be available for ${resourceName} values.`
            }</p>
          </React.Fragment>
        );
        this.setState({ lastErrorHtml });
      });
  }

  componentDidMount() {
    if(this.props.name) {
      this.fetchRecipeProperties();
    }

    ["ingredients", "units", "categories"].forEach((resourceName) => {
      this.fetchResourceList(resourceName);
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ formSubmissionInProgress: true });

    let submit;
    if(this.props.name) {
      submit = (data) => {
        return axios.put(
          `${settings.apiUrl}/recipes/${encodeURIComponent(this.props.name)}`,
          data
        );
      };
    } else {
      submit = (data) => axios.post(`${settings.apiUrl}/recipes/`, data);
    }

    submit({ recipe: this.state.recipe })
      .then(() => {
        this.props.history.push(`/recipes/${this.state.recipe.name}`);
      })
      .catch(error => {
        let lastErrorHtml;
        if(
          error.response &&
          error.response.data &&
          error.response.data.errors
        ) {
          const errorMessages = error.response.data.errors;
          const errorMessagesHtml = errorMessages.map(message => {
            return (<li key={message}>{message}</li>);
          });

          if(!isTest) {
            console.error(
`Errors from server in response to recipe create/edit form submission:
${errorMessages.join("\n")}`
            );
          }

          lastErrorHtml = (
            <React.Fragment>
              <p>Couldn&#39;t save recipe:</p>
              <ul>{errorMessagesHtml}</ul>
            </React.Fragment>
          );
        } else {
          const errorMessage = error.message || error.toString();
          lastErrorHtml = (<p>{`Couldn't save recipe: ${errorMessage}`}</p>);

          if(!isTest) {
            console.error(
              `Error submitting recipe create/edit form: ${errorMessage}`
            );
          }
        }

        if(!isTest) {
          // Jest's test runner doesn't implement window.scroll() so we have to
          // guard against calling it inside tests where it will crash the test
          window.scroll(0, 0);
        }
        this.setState({ lastErrorHtml, formSubmissionInProgress: false });
      });
  }

  handlePropertyChange(property) {
    return (newValue => {
      this.setState({
        recipe: {
          ...this.state.recipe,
          [property]: newValue,
        },
      });
    }).bind(this);
  }

  handleIngredientPreparationAddition() {
    const updatedRecipe = this.state.recipe;
    updatedRecipe.
      ingredient_preparations.
      push(this.defaultIngredientPreparation());
    this.setState({ recipe: updatedRecipe });
  }

  handleIngredientPreparationChange(newValue, index, property) {
    if(property === "amount") {
      if(newValue.trim() === "") {
        newValue = null;
      } else {
        newValue = parseFloat(newValue);
      }
    }
    const updatedRecipe = this.state.recipe;
    updatedRecipe.ingredient_preparations[index][property] = newValue;
    this.setState({ recipe: updatedRecipe });
  }

  handleIngredientPreparationRemoval(index) {
    const updatedRecipe = this.state.recipe;
    updatedRecipe.ingredient_preparations.splice(index, 1);
    this.setState({ recipe: updatedRecipe });
  }

  render() {
    let allDataLoaded = (
      this.state.ingredientsList &&
      this.state.unitsList &&
      this.state.categoriesList
    );
    if(this.props.name) {
      allDataLoaded = allDataLoaded && this.state.recipe;
    }

    if(allDataLoaded) {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <RecipeForm
            recipe={this.state.recipe}
            categoriesList={this.state.categoriesList}
            ingredientsList={this.state.ingredientsList}
            unitsList={this.state.unitsList}

            handleNameChange={this.handlePropertyChange("name")}
            handleDescriptionChange={this.handlePropertyChange("description")}
            handleCategoryChange={this.handlePropertyChange("category")}
            handleSourceChange=
              {this.handlePropertyChange("source")}
            handleIngredientPreparationChange=
              {this.handleIngredientPreparationChange}
            handleIngredientPreparationAddition=
              {this.handleIngredientPreparationAddition}
            handleIngredientPreparationRemoval=
              {this.handleIngredientPreparationRemoval}
            handleDirectionsChange={this.handlePropertyChange("directions")}
            handleCommentsChange={this.handlePropertyChange("comments")}
            handleSubmit={this.handleSubmit}

            formSubmissionInProgress={this.state.formSubmissionInProgress}
          />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <Loading />
        </React.Fragment>
      );
    }
  }
}

EditableRecipe.propTypes = {
  name: PropTypes.string,
  history: PropTypes.object.isRequired,
};

export default withRouter(EditableRecipe);
