import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCog } from '@fortawesome/free-solid-svg-icons';
library.add([faCog]);

const Loading = () => {

  return (
    <div className="component-loading hero is-medium">
      <div className="hero-body">
        <FontAwesomeIcon className="fa-3x fa-spin" icon="cog" />
        <p className="subtitle is-4">Loading...</p>
      </div>
    </div>
  );
};

export default React.memo(Loading);
