import React from 'react';
import PropTypes from 'prop-types';

const ErrorBox = ({errorMessageHtml}) => {
  if(errorMessageHtml) {
    return (
      <div className="error-box notification is-danger">
        {errorMessageHtml}
      </div>
    );
  } else {
    return null;
  }
};

ErrorBox.propTypes = {
  errorMessageHtml: PropTypes.element,
};

export default React.memo(ErrorBox);
