import React from 'react';
import axios from 'axios';

import settings from './../settings';
import Loading from './ui/loading';
import ErrorBox from './ui/error_box';
import IngredientCard from './ingredient_card';

const isTest = process && process.env && process.env.NODE_ENV === "test";

class IngredientsMostUsed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastErrorHtml: null,
      ingredients: null,
    };
  }

  componentDidMount() {
    this.fetchMostUsedIngredientList();
  }

  fetchMostUsedIngredientList() {
    axios
      .get(
        `${settings.apiUrl}/ingredients/`,
        { params: { most_used_count: 15 } }
      )
      .then(response => {
        const ingredients = response.data;
        this.setState({ ingredients });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching most-used ingredients from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const errorMessageHtml = (
          <p>
            Error fetching most-used ingredients from the server: {errorMessage}
          </p>
        );
        this.setState({ lastErrorHtml: errorMessageHtml });
      });
  }

  render() {
    if(this.state.ingredients) {
      const ingredientDisplay = this.state.ingredients.map(ingredient => {
        return (
          <IngredientCard
            name={ingredient.name}
            numRecipes={ingredient.num_recipes}
            key={ingredient.name}
          />
        );
      });

      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <section className="component-ingredient-list section">
            <ul>{ingredientDisplay}</ul>
          </section>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <Loading />
        </React.Fragment>
      );
    }
  }
}

IngredientsMostUsed.propTypes = {
};

export default IngredientsMostUsed;
