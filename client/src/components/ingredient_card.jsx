import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const IngredientCard = React.memo((props) => {

  const recipeListURL = `/recipes?ingredient=${props.name}`;

  let recipesLabel = "";
  if(props.numRecipes === 1) {
    recipesLabel = "recipe";
  } else {
    recipesLabel = "recipes";
  }

  return (
    <li
      className="component-ingredient-card card"
      key={props.name}
    >
      <Link to={recipeListURL} data-testid="card-link">
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <h1 className="title" data-testid="card-name">
                {props.name}
              </h1>
              <h2 className="subtitle" data-testid="card-recipe-count">
                {props.numRecipes} {recipesLabel}
              </h2>
            </div>
          </div>
        </div>
      </Link>
    </li>
  );
});

IngredientCard.displayName = "IngredientCard";

IngredientCard.propTypes = {
  name: PropTypes.string.isRequired,
  numRecipes: PropTypes.number.isRequired,
}

export default IngredientCard;
