import React from 'react';
import PropTypes from 'prop-types'
import { DebounceInput } from 'react-debounce-input';

const RecipesFilterControls = React.memo((props) => {
  const optionsFromList = (list) => {
    return list.map(item => (<option key={item}>{item}</option>));
  };

  const changeHandler = (callbackName) => {
    return (event) => props[callbackName](event.target.value);
  };

  const mobileControls = (
    <div
      className="component-recipes-filter-controls is-hidden-tablet"
      data-testid="component-recipes-filter-controls-mobile"
    >
      <div className="field is-horizontal">
        <div className="field-label">
          <label
            className="label"
            htmlFor="filter-controls-name-mobile"
          >
            Name:
          </label>
        </div>

        <div className="field-body">
          <div className="field">
            <div className="control">
              <DebounceInput
                className="input"
                type="text"
                id="filter-controls-name-mobile"
                value={props.recipeNameFilter}
                debounceTimeout={500}
                onChange={changeHandler("handleRecipeNameFilterChange")}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label">
          <label
            className="label"
            htmlFor="filter-controls-category-mobile"
          >
            Category:
          </label>
        </div>

        <div className="field-body">
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select
                  id="filter-controls-category-mobile"
                  value={props.categoryFilter}
                  onChange={changeHandler("handleCategoryFilterChange")}
                >
                  <option key=""></option>
                  {optionsFromList(props.categories)}
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label">
          <label
            className="label"
            htmlFor="filter-controls-ingredient-mobile"
          >
            Ingredient:
          </label>
        </div>

        <div className="field-body">
          <div className="field">
            <div className="control">
              <DebounceInput
                className="input"
                type="text"
                id="filter-controls-ingredient-mobile"
                value={props.ingredientFilter}
                debounceTimeout={500}
                onChange={changeHandler("handleIngredientFilterChange")}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label">
          <label
            className="label"
            htmlFor="filter-controls-tag-mobile"
          >
            Tag:
          </label>
        </div>

        <div className="field-body">
          <div className="field">
            <div className="control">
              <div className="select">
                <select
                  id="filter-controls-tag-mobile"
                  value={props.tagFilter}
                  onChange={changeHandler("handleTagFilterChange")}
                >
                  <option key=""></option>
                  {optionsFromList(props.tags)}
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  const tabletAndUpControls = (
    <div
      className="component-recipes-filter-controls is-hidden-mobile"
      data-testid="component-recipes-filter-controls-desktop"
    >
      <span className="field">
        <label
          className="label"
          htmlFor="filter-controls-name-desktop"
        >
          Name Filter:
        </label>
        <span className="control">
          <DebounceInput
            className="input"
            type="text"
            id="filter-controls-name-desktop"
            value={props.recipeNameFilter}
            debounceTimeout={500}
            onChange={changeHandler("handleRecipeNameFilterChange")}
          />
        </span>
      </span>

      <span className="field">
        <label
          className="label"
          htmlFor="filter-controls-category-desktop"
        >
          Category Filter:
        </label>
        <span className="control">
          <div className="select">
            <select
              id="filter-controls-category-desktop"
              value={props.categoryFilter}
              onChange={changeHandler("handleCategoryFilterChange")}
            >
              <option key=""></option>
              {optionsFromList(props.categories)}
            </select>
          </div>
        </span>
      </span>

      <span className="field">
        <label
          className="label"
          htmlFor="filter-controls-ingredient-desktop"
        >
          Ingredient Filter:
        </label>
        <span className="control">
          <DebounceInput
            className="input"
            type="text"
            id="filter-controls-ingredient-desktop"
            value={props.ingredientFilter}
            debounceTimeout={500}
            onChange={changeHandler("handleIngredientFilterChange")}
          />
        </span>
      </span>

      <span className="field">
        <label
          className="label"
          htmlFor="filter-controls-tag-desktop"
        >
          Tag Filter:
        </label>
        <span className="control">
          <div className="select">
            <select
              id="filter-controls-tag-desktop"
              value={props.tagFilter}
              onChange={changeHandler("handleTagFilterChange")}
            >
              <option key=""></option>
              {optionsFromList(props.tags)}
            </select>
          </div>
        </span>
      </span>
    </div>
  );

  return (
    <React.Fragment>
      {mobileControls}
      {tabletAndUpControls}
    </React.Fragment>
  );
});

RecipesFilterControls.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.string).isRequired,
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,

  recipeNameFilter: PropTypes.string.isRequired,
  categoryFilter: PropTypes.string.isRequired,
  ingredientFilter: PropTypes.string.isRequired,
  tagFilter: PropTypes.string.isRequired,

  handleRecipeNameFilterChange: PropTypes.func.isRequired,
  handleCategoryFilterChange: PropTypes.func.isRequired,
  handleIngredientFilterChange: PropTypes.func.isRequired,
  handleTagFilterChange: PropTypes.func.isRequired,
};

export default RecipesFilterControls;
