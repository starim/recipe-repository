import React from 'react';
import PropTypes from 'prop-types';

const RecipeFormCategory = React.memo((props) => {
  const options  = [(<option key="" value=""></option>)].concat(
    props.categoriesList.map((name) => {
      return (<option key={name} value={name}>{name}</option>);
    })
  );

  return (
    <div className="field">
      <label className="label" htmlFor="recipe-edit-category">Category</label>
      <div className="control">
        <div className="select">
          <select
            id="recipe-edit-category"
            value={props.selectedCategory}
            onChange={event => props.handleCategoryChange(event.target.value)}
          >
            {options}
          </select>
        </div>
      </div>
    </div>
  );
});

RecipeFormCategory.displayName = "RecipeFormCategory";

RecipeFormCategory.propTypes = {
  categoriesList: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedCategory: PropTypes.string.isRequired,
  handleCategoryChange: PropTypes.func.isRequired,
};

export default React.memo(RecipeFormCategory);
