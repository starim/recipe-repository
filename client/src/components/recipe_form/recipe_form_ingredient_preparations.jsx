import React from "react";
import PropTypes from "prop-types";

const RecipeFormIngredientPreparations = (props) => {
  const ingredientPreparationsHtml =
    props.ingredientPreparations.map((prep, index) => {

      const eventHandlerForPropertyChange = (property) => {
        return (event) => {
          return props.handleIngredientPreparationChange(
            event.target.value, index, property,
          );
        };
      };

      const eventHandlerForRemoveButtonClick = () => {
        return props.handleIngredientPreparationRemoval(index);
      };

      return (
        <div
          className="component-recipe-form-ip field is-horizontal"
          key={index}
          data-testid={`form-ingredient-preparations-input-${index}`}
        >
          <div className="field-label is-normal">
            <label className="label">#{index + 1}</label>
          </div>

          <div className="field-body">
            <div className="field">
              <p className="control is-expanded">
                <input
                  className="input"
                  type="number"
                  step="any" value={prep.amount || ""}
                  onChange={eventHandlerForPropertyChange("amount")}
                  data-testid="form-ingredient-preparations-amount"
                />
              </p>
            </div>

            <div className="field">
              <p className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  value={prep.unit}
                  placeholder="Unit"
                  onChange={eventHandlerForPropertyChange("unit")}
                />
              </p>
            </div>

            <div className="field">
              <p className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  value={prep.ingredient || ""}
                  placeholder="Ingredient"
                  onChange={eventHandlerForPropertyChange("ingredient")}
                />
              </p>
            </div>

            <div className="field">
              <p className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  value={prep.preparation || ""}
                  placeholder="Preparation"
                  onChange={eventHandlerForPropertyChange("preparation")}
                />
              </p>
            </div>
            <a
              data-testid={`form-ingredient-preparation-remove-${index}`}
              className="delete"
              onClick={eventHandlerForRemoveButtonClick}
            ></a>
          </div>

        </div>
      );
    });

  return (
    <div
      className="box"
      data-testid="form-ingredient-preparations"
    >
      <label className="label">Ingredients</label>
      {ingredientPreparationsHtml}
      <div className="field is-grouped is-grouped-right">
        <p className="control">
          <input
            className="button is-primary"
            type="button"
            value="Add Ingredient"
            onClick={() => props.handleIngredientPreparationAddition()}
          />
        </p>
      </div>
    </div>
  );
};

RecipeFormIngredientPreparations.propTypes = {
  ingredientPreparations: PropTypes.arrayOf(PropTypes.shape({
    preparation: PropTypes.string,
    amount: PropTypes.number,
    unit: PropTypes.string,
    ingredient: PropTypes.string,
  })),
  ingredientsList: PropTypes.arrayOf(PropTypes.string).isRequired,
  unitsList: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleIngredientPreparationChange: PropTypes.func.isRequired,
  handleIngredientPreparationAddition: PropTypes.func.isRequired,
  handleIngredientPreparationRemoval: PropTypes.func.isRequired,
};

export default RecipeFormIngredientPreparations;
