import React from 'react';
import PropTypes from 'prop-types';

class RecipeFormDirections extends React.PureComponent {
  constructor(props) {
    super(props);

    this.dispatchDirectionsChangeFromNew =
      this.dispatchDirectionsChangeFromNew.bind(this);
    this.dispatchDirectionsChangeFromEdit =
      this.dispatchDirectionsChangeFromEdit.bind(this);
    this.dispatchDirectionsChangeFromRemove =
      this.dispatchDirectionsChangeFromRemove.bind(this);
  }

  /* eslint no-unused-vars: "off" */
  dispatchDirectionsChangeFromNew(event) {
    this.props.handleDirectionsChange(this.props.directions.concat([""]));
  }

  dispatchDirectionsChangeFromRemove(index) {
    const newDirections = this.props.directions;
    newDirections.splice(index, 1);
    this.props.handleDirectionsChange(newDirections);
  }

  dispatchDirectionsChangeFromEdit(event, index) {
    const updatedDirections = this.props.directions;
    updatedDirections[index] = event.target.value;
    this.props.handleDirectionsChange(updatedDirections);
  }

  render() {
    const directionsInputs = this.props.directions.map((direction, index) => {

      const eventHandlerForDirectionChange = (event) => {
        this.dispatchDirectionsChangeFromEdit(event, index);
      };

      const eventHandlerForRemoveButtonClick = () => {
        this.dispatchDirectionsChangeFromRemove(index);
      };

      return (
        <div
          className="component-recipe-form-directions field is-horizontal"
          key={index}
        >
          <div className="field-label is-normal">
            <label
              className="label"
              htmlFor={`recipe-edit-direction-${index}`}
            >
              #{index + 1}
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control is-expanded">
                <textarea className="textarea"
                  id={`recipe-edit-direction-${index}`}
                  value={direction}
                  onChange={eventHandlerForDirectionChange}
                  data-testid={`form-directions-input-${index}`}
                />
              </div>
            </div>
            <a
              data-testid={`form-directions-remove-${index}`}
              className="delete"
              onClick={eventHandlerForRemoveButtonClick}
            ></a>
          </div>
        </div>
      );
    });

    return (
      <div
        className="box"
        data-testid="form-directions"
      >
        <label className="label">Directions</label>
        {directionsInputs}
        <div className="field is-grouped is-grouped-right">
          <p className="control">
            <input
              type="button"
              className="button is-primary"
              value="Add New Step"
              onClick={this.dispatchDirectionsChangeFromNew}
            />
          </p>
        </div>
      </div>
    );
  }
}

RecipeFormDirections.propTypes = {
  directions: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleDirectionsChange: PropTypes.func.isRequired,
};

export default RecipeFormDirections;
