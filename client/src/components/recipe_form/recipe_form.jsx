import React from 'react';
import PropTypes from 'prop-types';

import RecipeFormCategory from './recipe_form_category';
import RecipeFormDirections from './recipe_form_directions';
/* eslint max-len: "off" */
import RecipeFormIngredientPreparations from './recipe_form_ingredient_preparations';

const RecipeForm = React.memo((props) => {

  const submitButton = props.formSubmissionInProgress ?
    <input className="button is-primary" type="submit" disabled value="Saving..." />
    : <input className="button is-primary" type="submit" value="Save Recipe" />;

  return (
    <div data-testid="recipe-form" className="recipe-form box">
      <form onSubmit={props.handleSubmit}>
        <div className="field">
          <label className="label" htmlFor="recipe-edit-name">Name</label>
          <div className="control">
            <input
              id="recipe-edit-name"
              className="input"
              type="text"
              value={props.recipe.name}
              onChange={event => props.handleNameChange(event.target.value)}
            />
          </div>
        </div>
        <RecipeFormCategory
          selectedCategory={props.recipe.category}
          categoriesList={props.categoriesList}
          handleCategoryChange={props.handleCategoryChange}
        />
        <div className="field">
          <label className="label" htmlFor="recipe-edit-description">Description</label>
          <div className="control">
            <textarea
              id="recipe-edit-description"
              className="textarea"
              value={props.recipe.description}
              onChange={event => props.handleDescriptionChange(event.target.value)}
            />
          </div>
        </div>
        <div className="field">
          <label className="label" htmlFor="recipe-edit-source">Source</label>
          <div className="control">
            <input
              id="recipe-edit-source"
              className="input"
              type="text"
              value={props.recipe.source}
              onChange={event => props.handleSourceChange(event.target.value)}
            />
          </div>
        </div>
        <RecipeFormIngredientPreparations
          ingredientPreparations={props.recipe.ingredient_preparations}
          ingredientsList={props.ingredientsList}
          unitsList={props.unitsList}
          handleIngredientPreparationChange={props.handleIngredientPreparationChange}
          handleIngredientPreparationAddition={props.handleIngredientPreparationAddition}
          handleIngredientPreparationRemoval={props.handleIngredientPreparationRemoval}
        />
        <RecipeFormDirections
          directions={props.recipe.directions}
          handleDirectionsChange={props.handleDirectionsChange}
        />
        <div className="field">
          <label className="label" htmlFor="recipe-edit-comments">Comments</label>
          <div className="control">
            <textarea
              id="recipe-edit-comments"
              className="textarea"
              value={props.recipe.comments}
              onChange={event => props.handleCommentsChange(event.target.value)}
            />
          </div>
        </div>
        <div className="field is-grouped is-grouped-right">
          <p className="control">
            {submitButton}
          </p>
        </div>
      </form>
    </div>
  );
});

RecipeForm.displayName = "RecipeForm";

RecipeForm.propTypes = {
  recipe: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    category: PropTypes.string.isRequired,
    source: PropTypes.string.isRequired,
    ingredient_preparations: PropTypes.arrayOf(PropTypes.shape({
      preparation: PropTypes.string,
      amount: PropTypes.number,
      unit: PropTypes.string,
      ingredient: PropTypes.string,
    })),
    directions: PropTypes.arrayOf(PropTypes.string),
    comments: PropTypes.string,
  }).isRequired,

  categoriesList: PropTypes.arrayOf(PropTypes.string).isRequired,
  ingredientsList: PropTypes.arrayOf(PropTypes.string).isRequired,
  unitsList: PropTypes.arrayOf(PropTypes.string).isRequired,

  handleNameChange: PropTypes.func.isRequired,
  handleDescriptionChange: PropTypes.func.isRequired,
  handleCategoryChange: PropTypes.func.isRequired,
  handleSourceChange: PropTypes.func.isRequired,
  handleIngredientPreparationChange: PropTypes.func.isRequired,
  handleIngredientPreparationAddition: PropTypes.func.isRequired,
  handleIngredientPreparationRemoval: PropTypes.func.isRequired,
  handleDirectionsChange: PropTypes.func.isRequired,
  handleCommentsChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,

  formSubmissionInProgress: PropTypes.bool.isRequired,
};

export default React.memo(RecipeForm);
