import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBeer, faCookieBite } from '@fortawesome/free-solid-svg-icons';
library.add([faBeer, faCookieBite]);

// the amount property is a decimal value but it looks better to drop
// the decimal if it represents a whole number (e.g. 8.0 should be
// displayed as 8)
const prettify = (amount) => {
  if(amount) {
    return parseInt(amount) === parseFloat(amount) ? parseInt(amount) : amount;
  } else {
    return "";
  }
}

const RecipeFullDetails = React.memo((props) => {

  const ingredientsHtml = props.recipe.ingredient_preparations.map(prep => {
    const prettyAmount = prep.amount? `${prettify(prep.amount)} ` : "";
    const unitText = prep.unit ? `${prep.unit} of ` : "";
    const preparationText = prep.preparation ? `, ${prep.preparation}`: "";

    return (
      <li key={prep.ingredient} className="recipe-details-ingredient">
        {prettyAmount}{unitText}{prep.ingredient}{preparationText}
      </li>
    );
  });

  const directionsHtml = props.recipe.directions.map((step) => {
    return (
      <li key={step} className="recipe-details-direction">
        {step}
      </li>
    );
  });

  const tagsHtml = (props.recipe.tags || []).map((tag) => {
    return (
      <li key={tag} className="">
        <span className="tag is-primary is-medium">{tag}</span>
      </li>
    );
  });

  const categoryHtml = (
    <div>
      <div>
        <p className="heading">Category</p>
        <p className="subtitle">
          <FontAwesomeIcon
            className="category-icon"
            icon={props.recipe.category === 'Drink' ? 'beer' : 'cookie-bite'}
            data-category={props.recipe.category}
          />
        </p>
      </div>
    </div>
  );

  let sourceHtml;
  if(props.recipe.source) {
    sourceHtml = (
      <div>
        <p className="heading">Source</p>
        <p
          className="subtitle is-italic"
          data-testid="recipe-source-text"
        >{props.recipe.source}</p>
      </div>
    );
  }

  let commentHtml;
  if(props.recipe.comments) {
    commentHtml = (
      <div className="content">
        <h1 className="title is-4">Comments</h1>
        <p>{props.recipe.comments}</p>
      </div>
    );
  }

  return (
    <div
      className="component-recipe-full-details box"
      data-testid="recipe-show"
    >
      <section className="hero content is-primary">
        <div className="hero-body">
          <div className="container">
            <h1
              className="title has-text-centered"
              data-testid="full-details-name"
            >
              {props.recipe.name}
            </h1>
            <nav className="level">
              <div className="level-item has-text-centered">
                <div>
                  {sourceHtml}
                </div>
              </div>
              <div className="level-item has-text-centered">
                <div>
                  {categoryHtml}
                </div>
              </div>
            </nav>
          </div>
        </div>
      </section>

      <div className="content">
        <p
          data-testid="recipe-description"
        >{props.recipe.description}</p>
      </div>

      <div className="content">
        <h1 className="title is-4">Ingredients</h1>
        <ul>{ingredientsHtml}</ul>
      </div>

      <div className="content">
        <h1 className="title is-4">Directions</h1>
        <ol>{directionsHtml}</ol>
      </div>

      <div>
        <h1 className="title is-4">Tags</h1>
        <ul>{tagsHtml}</ul>
      </div>

      {commentHtml}
    </div>
  );
});

RecipeFullDetails.displayName = "RecipeFullDetails";

RecipeFullDetails.propTypes = {
  recipe: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    source: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string).isRequired,
    ingredient_preparations: PropTypes.arrayOf(PropTypes.shape({
      preparation: PropTypes.string,
      amount: PropTypes.number,
      unit: PropTypes.string,
      ingredient: PropTypes.string.isRequired,
    })).isRequired,
    directions: PropTypes.arrayOf(PropTypes.string).isRequired,
    comments: PropTypes.string.isRequired,
  }).isRequired,
};

export default RecipeFullDetails;
