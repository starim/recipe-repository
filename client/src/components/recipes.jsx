import React from 'react';
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import axios from 'axios';
const parseDecimalNumber = require('parse-decimal-number');

import settings from './../settings';
import Loading from './ui/loading';
import RecipesFilterControls from './recipes_filter_controls';
import RecipeList from './recipe_list';
import ErrorBox from './ui/error_box';

const isTest = process && process.env && process.env.NODE_ENV === "test";

class Recipes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastErrorHtml: null,
      recipes: null,
      categories: null,
      tags: null,
      filters: props.initialFilters,
    };
  }

  componentDidMount() {
    this.fetchRecipeList(this.state.filters);
    ["categories", "tags"].forEach((resourceName) => {
      this.fetchResourceList(resourceName);
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.filters !== this.state.filters) {
      this.fetchRecipeList(this.state.filters);
    }
  }

  fetchRecipeList(filters) {
    axios
      .get(`${settings.apiUrl}/recipes/`, {
        params: {
          recipe_name_filter: filters.recipeName,
          category_filter: filters.category,
          ingredient_filter: filters.ingredient,
          tag_filter: filters.tag,
        },
      })
      .then(response => {
        // Ruby serializes each ingredient_preparations' amount field
        // as a string since there's no decimal type in JSON, so we
        // have to manually deserialize it
        const recipes = response.data.map(recipe => {
          const convertedRecipe = Object.assign({}, recipe);
          convertedRecipe.ingredient_preparations =
            recipe.ingredient_preparations.map(prep => {
              const amount =
                prep.amount ? parseDecimalNumber(prep.amount) : null;
              return {
                ...prep,
                amount,
              };
            });
          return convertedRecipe;
        });

        this.setState({ recipes });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching recipe list from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const errorMessageHtml = (
          <p>Error fetching recipe list from the server: {errorMessage}</p>
        );
        this.setState({ lastErrorHtml: errorMessageHtml });
      });
  }

  fetchResourceList(resourceName) {
    axios
      .get(`${settings.apiUrl}/${resourceName}/`)
      .then(response => {
        const names = response.data.map(object => object.name);
        this.setState({ [resourceName]: names });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching resource ${resourceName} from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const lastErrorHtml = (
          <React.Fragment>
            <p>{
              `Error fetching ${resourceName} list from server: ${errorMessage}`
            }</p>
            <p>{
              `Autocomplete won't be available for ${resourceName} values.`
            }</p>
          </React.Fragment>
        );
        this.setState({ lastErrorHtml });
      });
  }

  handleFilterChange(property) {
    return (newValue => {

      const updatedFilters = {
        ...this.state.filters,
        [property]: newValue,
      };

      const serializedFilters =
        Object.keys(updatedFilters)
        .filter(property => updatedFilters[property])
        .map(property => {
          const sanitizedProperty = encodeURIComponent(property);
          const sanitizedValue = encodeURIComponent(updatedFilters[property]);
          return `${sanitizedProperty}=${sanitizedValue}`;
        })
        .join("&");

      this.props.history.replace(
        {
          pathname: this.props.history.location.pathname,
          search: "?" + serializedFilters,
        },
      );

      this.setState({ filters: updatedFilters });
    }).bind(this);
  }

  render() {
    let allDataLoaded = (
      this.state.recipes &&
      this.state.categories &&
      this.state.tags
    );

    if(allDataLoaded) {
      return (
        <React.Fragment>
          <RecipesFilterControls
            categories={this.state.categories}
            tags={this.state.tags}

            recipeNameFilter={this.state.filters.recipeName}
            categoryFilter={this.state.filters.category}
            ingredientFilter={this.state.filters.ingredient}
            tagFilter={this.state.filters.tag}

            handleRecipeNameFilterChange=
              {this.handleFilterChange("recipeName")}
            handleCategoryFilterChange=
              {this.handleFilterChange("category")}
            handleIngredientFilterChange=
              {this.handleFilterChange("ingredient")}
            handleTagFilterChange=
              {this.handleFilterChange("tag")}
          />
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <RecipeList recipes={this.state.recipes} />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <Loading />
        </React.Fragment>
      );
    }
  }
}

Recipes.propTypes = {
  initialFilters: PropTypes.shape({
    recipeName: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    ingredient: PropTypes.string.isRequired,
    tag: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(Recipes);
