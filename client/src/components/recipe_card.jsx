import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const RecipeCard = React.memo((props) => {

  const ingredientListHtml = props.ingredient_preparations.map((prep) => {
    return (
      <li key={prep.ingredient} className="tag is-primary is-light">
        { prep.ingredient }
      </li>
      );
    });

  const tagListHtml = (props.tags || []).map((tag) => {
    return (
      <li key={tag}>
        <span className="tag is-primary is-medium">{tag}</span>
      </li>
      );
    });

  return (
    <li
      className="component-recipe-card card"
      key={props.name}
    >
      <Link to={`/recipes/${props.name}`} data-testid="card-link">
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <h1 className="title is-4 is-left" data-testid="card-name">
                {props.name}
              </h1>
              <p className="content" data-testid="card-description">
                {props.description}
              </p>
              <ul className="content" data-testid="card-ingredients-list">
                {ingredientListHtml}
              </ul>
              <ul className="content" data-testid="card-tags-list">
                {tagListHtml}
              </ul>
            </div>
          </div>
        </div>
      </Link>
    </li>
  );
});

RecipeCard.displayName = "RecipeCard";

RecipeCard.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  source: PropTypes.string,
  ingredient_preparations: PropTypes.arrayOf(PropTypes.shape({
    amount: PropTypes.number,
    unit: PropTypes.string,
    ingredient: PropTypes.string.isRequired,
  })).isRequired,
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
  directions: PropTypes.arrayOf(PropTypes.string).isRequired,
  comments: PropTypes.string.isRequired,
};

export default RecipeCard;
