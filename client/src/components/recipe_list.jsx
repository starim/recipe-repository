import React from 'react';
import PropTypes from 'prop-types';

import RecipeCard from './recipe_card';

const RecipeList = React.memo((props) => {

  const recipeCards = props.recipes.map(recipe => {
    return (<RecipeCard key={recipe.name} {...recipe} />);
  });

  return (
    <div
      className="component-recipe-list"
      data-testid="recipe-index"
    >
      <ul>
        { recipeCards }
      </ul>
    </div>
  );
});

RecipeList.displayName = "RecipeList";

RecipeList.propTypes = {
  recipes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      source: PropTypes.string,
      tags: PropTypes.arrayOf(PropTypes.string).isRequired,
      ingredient_preparations: PropTypes.arrayOf(PropTypes.shape({
        amount: PropTypes.number,
        unit: PropTypes.string,
        ingredient: PropTypes.string.isRequired,
      })).isRequired,
      directions: PropTypes.arrayOf(PropTypes.string).isRequired,
      comments: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};

export default RecipeList;
