import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
const parseDecimalNumber = require('parse-decimal-number');

import settings from './../settings';
import Loading from './ui/loading';
import RecipeFullDetails from './recipe_full_details';
import ErrorBox from './ui/error_box';

const isTest = process && process.env && process.env.NODE_ENV === "test";

class Recipe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastErrorHtml: null,
      recipe: null,
      ingredientsList: null,
      tags: null,
      unitsList: null,
      categoriesList: null,
    };
  }

  componentDidMount() {
    axios
      .get(`${settings.apiUrl}/recipes/${encodeURIComponent(this.props.name)}`)
      .then(response => {
        const recipe = Object.assign({}, response.data);
        recipe.ingredient_preparations =
          recipe.ingredient_preparations.map(prep => {
            const amount = prep.amount ? parseDecimalNumber(prep.amount) : null;
            return {
              ...prep,
              amount,
            };
          });
        this.setState({ recipe });
      })
      .catch(error => {
        if(!isTest) {
          console.error(`Error fetching recipe from server:`);
          console.error(error);
        }
        const errorMessage = error.message || error.toString();
        const errorMessageHtml = (
          <p>Error fetching recipe from the server: {errorMessage}</p>
        );
        this.setState({ lastErrorHtml: errorMessageHtml });
      });
  }

  render() {
    if(this.state.recipe) {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <RecipeFullDetails
            recipe={this.state.recipe}
            ingredientsList={this.state.ingredientsList}
            tags={this.state.tags}
            unitsList={this.state.unitsList}
            categoriesList={this.state.categoriesList}
          />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <ErrorBox errorMessageHtml={this.state.lastErrorHtml} />
          <Loading />
        </React.Fragment>
      );
    }
  }
}

Recipe.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Recipe;
