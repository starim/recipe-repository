import React from 'react';
import { Link, Route, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faScroll, faPlus, faEgg} from '@fortawesome/free-solid-svg-icons';
library.add([faScroll, faPlus, faEgg]);

import EditRecipeButton from "./edit_recipe_button";

const NavBar = () => {
  /* eslint react/prop-types: "off" */
  const editRecipeAdapter = ({match}) => {
    return (<EditRecipeButton recipeName={match.params.name} />);
  };

  const isRecipesRoute = () => {
    const location = useLocation();
    return location.pathname === "/recipes"
  }

  const isNewRecipeRoute = () => {
    const location = useLocation();
    return location.pathname === "/recipes/new"
  }

  const isIngredientsRoute = () => {
    const location = useLocation();
    return location.pathname === "/ingredients/most-used"
  }

  return (
    <div className="component-nav-bar">
      <div className="columns">
        <div className="column has-text-centered">
          <Link
            to="/recipes"
            className={isRecipesRoute() ? "is-primary button" : "button"}
          >
            <span className="icon">
              <FontAwesomeIcon
                className="fa category-icon"
                icon="scroll"
              />
            </span>
            <span>All Recipes</span>
          </Link>
        </div>
        <div className="column has-text-centered">
          <Link
            to="/recipes/new"
            className={isNewRecipeRoute() ? "is-primary button" : "button"}
            >
              <span className="icon">
                <FontAwesomeIcon
                  className="fa category-icon"
                  icon="plus"
                />
              </span>
              <span>New Recipe</span>
          </Link>
        </div>
        <div className="column has-text-centered">
          <Link
            to="/ingredients/most-used"
            className={isIngredientsRoute() ? "is-primary button" : "button"}
            >
              <span className="icon">
                <FontAwesomeIcon
                  className="fa category-icon"
                  icon="egg"
                />
              </span>
              <span>Ingredients</span>
          </Link>
        </div>
        <Route
          path="/recipes/:name"
          render={editRecipeAdapter}
        />
      </div>
      <hr/>
    </div>
  );
};

NavBar.propTypes = {
};

export default NavBar;
