import React from 'react';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
library.add([faEdit]);

const EditRecipeButton = ({recipeName}) => {

  const url = `/recipes/edit/${recipeName}`;

  const isRecipeEditRoute = () => {
    const location = useLocation();
    return location.pathname.includes("/recipes/edit/");
  }

  if(recipeName === "new") {
    return null;
  } else {
    return (
      <div className="column has-text-centered">
        <Link
          to={url}
          className={isRecipeEditRoute() ? "is-primary button" : "button"}
          >
            <span className="icon">
              <FontAwesomeIcon
                className="fa category-icon"
                icon="edit"
              />
            </span>
            <span>Edit Recipe</span>
        </Link>
      </div>
    );
  }
};

EditRecipeButton.propTypes = {
  recipeName: PropTypes.string.isRequired,
};

export default React.memo(EditRecipeButton);
