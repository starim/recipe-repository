import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import queryString from 'query-string';

import Recipes from './components/recipes';
import Recipe from './components/recipe';
import EditableRecipe from './components/editable_recipe';
import NavBar from './components/nav_bar/nav_bar';
import IngredientsMostUsed from './components/ingredients_most_used';

const App = () => {

  const redirectToIndexRoute = withRouter(({history}) => {
    history.push("/recipes");
    return (<div>Redirecting to /recipes</div>);
  });

  /* eslint react/prop-types: "off" */
  const recipesAdapter = withRouter(({history}) => {
    const filters = {
      recipeName: "",
      category: "",
      ingredient: "",
      tag: "",
    };
    if(history.location.search) {
      const queryState = queryString.parse(history.location.search);
      Object.keys(queryState).forEach(property => {
        if(Object.keys(filters).includes(property)) {
          filters[property] = queryState[property]
        }
      });
    }

    return (<Recipes initialFilters={filters} />);
  });

  /* eslint react/prop-types: "off" */
  const recipeAdapter = ({match}) => {
    return (<Recipe name={match.params.name} />);
  };

  /* eslint react/prop-types: "off" */
  const editableRecipeAdapter = ({match}) => {
    return (<EditableRecipe name={match.params.name} />);
  };

  return (
    <div className="component-app section">
      <NavBar />
      <Switch>
        <Route
          path="/"
          exact
          component={redirectToIndexRoute}
        />
        <Route
          path="/recipes"
          exact
          component={recipesAdapter}
          />
        <Route
          path="/recipes/new"
          component={() => (<EditableRecipe />)}
        />
        <Route
          path="/recipes/edit/:name"
          component={editableRecipeAdapter}
        />
        <Route
          path="/recipes/:name"
          component={recipeAdapter} />
        <Route
          path="/ingredients/most-used"
          component={IngredientsMostUsed}
        />
        <Route
          path="*"
          component={redirectToIndexRoute}
        />
      </Switch>
    </div>
  );
};

App.propTypes = {
};

export default App;
