import React from 'react'
import axios from 'axios';
import {MemoryRouter} from 'react-router-dom';
import {render, fireEvent, waitForElement, waitForDomChange, within} from 'react-testing-library';

import Recipes from 'components/recipes';

const testBackendUrl = "http://localhost:3000/api";

const recipes = [
  {
    name: "Pumpkin Pie",
    description: "An autumn dessert.",
    category: "Food",
    source: "The Internet",
    tags: [
      "Dessert",
    ],
    ingredient_preparations: [
      {
        preparation: "baked",
        amount: "9.0",
        unit: "in",
        ingredient: "pie crust",
      },
      {
        preparation: null,
        amount: "15.5",
        unit: "oz",
        ingredient: "pumpkin puree",
      },
      {
        preparation: null,
        amount: "1.0",
        unit: null,
        ingredient: "can of condensed milk",
      },
    ],
    directions: [
      "Mix pumpkin puree and condensed milk.",
      "Fill pie crust with pumpkin mix.",
      "Bake at 425° for 30 min.",
    ],
    comments: "Came out undercooked last time, bake for an additional five minutes.",
  },
  {
    name: "Pancakes",
    description: "A hearty breakfast.",
    category: "Food",
    source: "A friend.",
    tags: [
      "Breakfast",
    ],
    ingredient_preparations: [
      {
        preparation: "",
        amount: "4",
        unit: "oz",
        ingredient: "flour",
      },
      {
        preparation: "sliced",
        amount: "0.5",
        unit: "",
        ingredient: "apples",
      },
    ],
    directions: [
      "Heat stove.",
      "Cook ingredients.",
    ],
    comments: "Try adding cinnamon in the future.",
  },
];

const filteredRecipes = recipes.slice(1);

const categories = [
  { id: 1, name: "Breakfast" },
  { id: 2, name: "Lunch" },
  { id: 3, name: "Dinner" },
];

const tags = [
  { id: 1, name: "Favorite" },
  { id: 2, name: "Easy" },
];

const getForm =
  dom => dom.getByTestId("component-recipes-filter-controls-mobile");

const initialFilters = {
  recipeName: "",
  category: "",
  ingredient: "",
  tag: "",
};

jest.mock("axios");

describe('Recipes when there are filters and the viewport is phone-sized', () => {

  it("only shows on mobile", async () => {
    axios.get
      .mockResolvedValueOnce({ data: recipes })
      .mockResolvedValueOnce({ data: categories })
      .mockResolvedValueOnce({ data: tags });

    const dom = render(
      <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
    );
    const form = await waitForElement(() => getForm(dom));

    expect(form.className.split(" ")).toContain("is-hidden-tablet");
  });

  it("displays the filtered recipes only", async () => {
    axios.get
      .mockResolvedValueOnce({ data: recipes })
      .mockResolvedValueOnce({ data: categories })
      .mockResolvedValueOnce({ data: tags })
      .mockResolvedValueOnce({ data: filteredRecipes });

    const dom = render(
      <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
    );
    const form = await waitForElement(() => getForm(dom));

    const input = await waitForElement(
      () => within(form).getByLabelText("Name", { exact: false })
    );
    fireEvent.change(input, {target: {value: "cheese"}});
    await waitForDomChange(dom);

    const recipeNames = dom.getAllByTestId("card-name");
    expect(recipeNames.length).toBe(1);
    expect(recipeNames[0]).toHaveTextContent("Pancakes");
  });

  describe("name filter", () => {

    it("sends the requested filter parameter with recipes requests", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags })
        .mockResolvedValueOnce({ data: filteredRecipes });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Name", { exact: false })
      );
      fireEvent.change(input, {target: {value: "cheese"}});
      await waitForDomChange(dom);

      expect(axios.get).toHaveBeenLastCalledWith(
        `${testBackendUrl}/recipes/`,
        {
          params: {
            recipe_name_filter: "cheese",
            category_filter: "",
            ingredient_filter: "",
            tag_filter: "",
          },
        },
      );
    });
  });

  describe("category filter", () => {

    it("displays the existing categories as options", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Category", { exact: false })
      );

      const optionValues = Array.from(input.children).map(option => option.value);
      expect(optionValues).toEqual(["", "Breakfast", "Lunch", "Dinner"]);
    });

    it("sends the requested filter parameter with recipes requests", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags })
        .mockResolvedValueOnce({ data: filteredRecipes });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Category", { exact: false })
      );
      fireEvent.change(input, {target: {value: "Breakfast"}});
      await waitForDomChange(dom);

      expect(axios.get).toHaveBeenLastCalledWith(
        `${testBackendUrl}/recipes/`,
        {
          params: {
            recipe_name_filter: "",
            category_filter: "Breakfast",
            ingredient_filter: "",
            tag_filter: "",
          },
        },
      );
    });
  });

  describe("ingredient filter", () => {

    it("sends the requested filter parameter with recipes requests", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags })
        .mockResolvedValueOnce({ data: filteredRecipes });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Ingredient", { exact: false })
      );
      fireEvent.change(input, {target: {value: "cheese"}});
      await waitForDomChange(dom);

      expect(axios.get).toHaveBeenLastCalledWith(
        `${testBackendUrl}/recipes/`,
        {
          params: {
            recipe_name_filter: "",
            category_filter: "",
            ingredient_filter: "cheese",
            tag_filter: "",
          },
        },
      );
    });
  });

  describe("tag filter", () => {

    it("displays the existing tags as options", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Tag", { exact: false })
      );

      const optionValues = Array.from(input.children).map(option => option.value);
      expect(optionValues).toEqual(["", "Favorite", "Easy"]);
    });

    it("sends the requested filter parameter with recipes requests", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags })
        .mockResolvedValueOnce({ data: filteredRecipes });

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const form = await waitForElement(() => getForm(dom));

      const input = await waitForElement(
        () => within(form).getByLabelText("Tag", { exact: false })
      );
      fireEvent.change(input, {target: {value: "Favorite"}});
      await waitForDomChange(dom);

      expect(axios.get).toHaveBeenLastCalledWith(
        `${testBackendUrl}/recipes/`,
        {
          params: {
            recipe_name_filter: "",
            category_filter: "",
            ingredient_filter: "",
            tag_filter: "Favorite",
          },
        },
      );
    });
  });
});
