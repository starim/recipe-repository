import React from 'react';
import axios from 'axios';
import {MemoryRouter} from 'react-router-dom';
import {render, fireEvent, waitForDomChange, waitForElement, within} from 'react-testing-library';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from "history";
import queryString from 'query-string';

import App from 'app';
import Recipes from 'components/recipes';

const testBackendUrl = "http://localhost:3000/api";

const initialFilters = {
  recipeName: "",
  category: "",
  ingredient: "",
  tag: "",
};

jest.mock("axios");

describe('Recipes interaction with the URL query parameters', () => {

  describe("when there are URL query parameters specifying filters to use", () => {
    it("sets the inital filter state from the query paramters", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes?recipeName=a&category=b&ingredient=c&tag=d"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );
      await waitForDomChange(dom);

      expect(history.location.pathname).toBe("/recipes");

      const recipesCallParams = axios.get.mock.calls.find(args => {
        return (args[0] === `${testBackendUrl}/recipes/`);
      });
      expect(recipesCallParams[1]).toEqual({
        params: {
          recipe_name_filter: "a",
          category_filter: "b",
          ingredient_filter: "c",
          tag_filter: "d",
        },
      });
    });
  });

  describe("when there are no URL query parameters", () => {
    it("doesn't activate any filters", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );
      await waitForDomChange(dom);

      expect(history.location.pathname).toBe("/recipes");

      const recipesCallParams = axios.get.mock.calls.find(args => {
        return (args[0] === `${testBackendUrl}/recipes/`);
      });
      expect(recipesCallParams[1]).toEqual({
        params: {
          recipe_name_filter: "",
          category_filter: "",
          ingredient_filter: "",
          tag_filter: "",
        },
      });
    });
  });

  describe("when there are unrelated URL query parameters", () => {
    it("ignores the unrelated parameters and doesn't activate any filters", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes?someParam=x&otherParam=y"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );
      await waitForDomChange(dom);

      expect(history.location.pathname).toBe("/recipes");

      const recipesCallParams = axios.get.mock.calls.find(args => {
        return (args[0] === `${testBackendUrl}/recipes/`);
      });
      expect(recipesCallParams[1]).toEqual({
        params: {
          recipe_name_filter: "",
          category_filter: "",
          ingredient_filter: "",
          tag_filter: "",
        },
      });
    });
  });

  describe("when the name filter is used", () => {
    it("updates the URL with the query parameter for this filter", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      const form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      const input = await waitForElement(
        () => within(form).getByLabelText("Name Filter", { exact: false })
      );
      fireEvent.change(input, {target: {value: "cheese"}});
      await waitForDomChange(dom);

      expect(history.location.pathname).toBe("/recipes");
      const queryState = queryString.parse(history.location.search);
      expect(queryState).toEqual({ recipeName: "cheese" });
    });
  });

  describe("when the category filter is used", () => {
    it("updates the URL with the query parameter for this filter", async () => {
      axios.get
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [
          { id: 1, name: "Breakfast" },
          { id: 2, name: "Lunch" },
          { id: 3, name: "Dinner" },
        ]})
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      const form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      const input = await waitForElement(
        () => within(form).getByLabelText("Category Filter", { exact: false })
      );
      fireEvent.change(input, {target: {value: "Breakfast"}});

      expect(history.location.pathname).toBe("/recipes");
      const queryState = queryString.parse(history.location.search);
      expect(queryState).toEqual({ category: "Breakfast" });
    });
  });

  describe("when the ingredient filter is used", () => {
    it("updates the URL with the query parameter for this filter", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      const form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      const input = await waitForElement(
        () => within(form).getByLabelText("Ingredient Filter", { exact: false })
      );
      fireEvent.change(input, {target: {value: "cheese"}});
      await waitForDomChange(dom);

      expect(history.location.pathname).toBe("/recipes");
      const queryState = queryString.parse(history.location.search);
      expect(queryState).toEqual({ ingredient: "cheese" });
    });
  });

  describe("when the tag filter is used", () => {
    it("updates the URL with the query parameter for this filter", async () => {
      axios.get
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [
          { id: 1, name: "Breakfast" },
          { id: 2, name: "Lunch" },
          { id: 3, name: "Dinner" },
        ]})
        .mockResolvedValueOnce({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      const form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      const input = await waitForElement(
        () => within(form).getByLabelText("Tag Filter", { exact: false })
      );
      fireEvent.change(input, {target: {value: "Breakfast"}});

      expect(history.location.pathname).toBe("/recipes");
      const queryState = queryString.parse(history.location.search);
      expect(queryState).toEqual({ tag: "Breakfast" });
    });
  });

  describe("when all filter types are used", () => {
    it("updates the URL with the query parameters for all filters", async () => {
      axios.get
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [
          { id: 1, name: "Breakfast" },
          { id: 2, name: "Lunch" },
          { id: 3, name: "Dinner" },
        ]})
        .mockResolvedValueOnce({ data: [
          { id: 1, name: "Favorite" },
          { id: 2, name: "Easy" },
        ]})
        .mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      let form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      let input = await waitForElement(
        () => within(form).getByLabelText("Name Filter", { exact: false })
      );
      fireEvent.change(input, {target: {value: "burrito"}});

      await waitForDomChange(dom);
      form = dom.getByTestId("component-recipes-filter-controls-desktop");
      input = within(form).getByLabelText("Category Filter", { exact: false });
      fireEvent.change(input, {target: {value: "Breakfast"}});

      form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      input = within(form).getByLabelText("Ingredient Filter", { exact: false });
      fireEvent.change(input, {target: {value: "egg"}});

      await waitForDomChange(dom);
      form = await waitForElement(() => {
        return dom.getByTestId("component-recipes-filter-controls-desktop");
      });
      input = within(form).getByLabelText("Tag Filter", { exact: false });
      fireEvent.change(input, {target: {value: "Favorite"}});

      expect(history.location.pathname).toBe("/recipes");
      const queryState = queryString.parse(history.location.search);
      expect(queryState).toEqual({
        recipeName: "burrito",
        category: "Breakfast",
        ingredient: "egg",
        tag: "Favorite",
      });
    });
  });
});
