import React from 'react'
import axios from 'axios';
import {MemoryRouter, Router} from 'react-router-dom';
import {render, fireEvent, waitForElement, waitForDomChange, within} from 'react-testing-library';
import {createMemoryHistory} from "history";

import Recipes from 'components/recipes';
import App from 'app';

const recipes = [
  {
    name: "Pumpkin Pie",
    description: "An autumn dessert.",
    category: "Food",
    source: "The Internet",
    tags: [
      "Dessert",
    ],
    ingredient_preparations: [
      {
        preparation: "baked",
        amount: "9.0",
        unit: "in",
        ingredient: "pie crust",
      },
      {
        preparation: null,
        amount: "15.5",
        unit: "oz",
        ingredient: "pumpkin puree",
      },
      {
        preparation: null,
        amount: "1.0",
        unit: null,
        ingredient: "can of condensed milk",
      },
    ],
    directions: [
      "Mix pumpkin puree and condensed milk.",
      "Fill pie crust with pumpkin mix.",
      "Bake at 425° for 30 min.",
    ],
    comments: "Came out undercooked last time, bake for an additional five minutes.",
  },
  {
    name: "Pancakes",
    description: "A hearty breakfast.",
    category: "Food",
    source: "A friend.",
    tags: [
      "Breakfast",
    ],
    ingredient_preparations: [
      {
        preparation: "",
        amount: "4",
        unit: "oz",
        ingredient: "flour",
      },
      {
        preparation: "sliced",
        amount: "0.5",
        unit: "",
        ingredient: "apples",
      },
    ],
    directions: [
      "Heat stove.",
      "Cook ingredients.",
    ],
    comments: "Try adding cinnamon in the future.",
  },
];

const categories = [
  { id: 1, name: "Breakfast" },
  { id: 2, name: "Lunch" },
  { id: 3, name: "Dinner" },
];

const tags = [
  { id: 1, name: "Favorite" },
  { id: 2, name: "Easy" },
];

jest.mock("axios");
jest.spyOn(axios, "get");

describe('Recipes when there are no filters', () => {

  const initialFilters = {
    recipeName: "",
    category: "",
    ingredient: "",
    tag: "",
  };

  describe("when server returns the recipe list successfully", () => {

    beforeEach(() => {
      axios.get
        .mockResolvedValue({ data: recipes })
        .mockResolvedValueOnce({ data: recipes })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags });
    });

    it("displays the recipes' names", async () => {
      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const displayedNames  = await waitForElement(
        () => dom.getAllByTestId("card-name")
      );
      expect(displayedNames.length).toBe(2);
      expect(displayedNames[0]).toHaveTextContent("Pumpkin Pie");
      expect(displayedNames[1]).toHaveTextContent("Pancakes");
    });

    it("displays the recipes' descriptions", async () => {
      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const displayedDescriptions  = await waitForElement(
        () => dom.getAllByTestId("card-description")
      );
      expect(displayedDescriptions.length).toBe(2);
      expect(displayedDescriptions[0]).toHaveTextContent("An autumn dessert.");
      expect(displayedDescriptions[1]).toHaveTextContent("A hearty breakfast.");
    });

    it("displays the recipes' ingredients", async () => {
      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );
      const ingredientNodes  = await waitForElement(
        () => dom.getAllByTestId("card-ingredients-list")
      );
      expect(ingredientNodes.length).toBe(2);
      expect(ingredientNodes[0]).toHaveTextContent("pie crust");
      expect(ingredientNodes[0]).toHaveTextContent("pumpkin puree");
      expect(ingredientNodes[0]).toHaveTextContent("can of condensed milk");
      expect(ingredientNodes[1]).toHaveTextContent("flour");
      expect(ingredientNodes[1]).toHaveTextContent("apples");
    });

    it("links to the recipe detail page", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });
      const dom = render(<Router history={history}><App /></Router>);
      const recipeCards  = await waitForElement(
        () => dom.getAllByTestId("card-link")
      );
      expect(recipeCards.length).toBe(2);
      fireEvent.click(recipeCards[1], {});
      await waitForDomChange();

      expect(history.location.pathname).toEqual("/recipes/Pancakes");
    });
  });

  describe("when there's an error fetching the recipe data", () => {
    it("should display the error", async () => {
      axios.get
        .mockRejectedValue({ message: "server's on fire" })
        .mockRejectedValueOnce({ message: "server's on fire" })
        .mockResolvedValueOnce({ data: categories })
        .mockResolvedValueOnce({ data: tags })

      const dom = render(
        <MemoryRouter><Recipes initialFilters={initialFilters} /></MemoryRouter>
      );

      await waitForElement(
        () => dom.getByText("server's on fire", { exact: false })
      );
    });
  });

  // TODO: add tests that clicking on a recipe card loads the recipe show page
});
