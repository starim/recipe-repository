import React from 'react';
import axios from 'axios';
import {render, waitForElement, within} from 'react-testing-library';
import {MemoryRouter, Route, Switch} from 'react-router-dom';

import Recipe from 'components/recipe'

jest.mock('axios');
jest.spyOn(axios, "get");

describe("Recipe", () => {

  describe("when the server returns an error when requesting the recipe data", () => {

    const recipeName = "pie";

    it("should display the error", async () => {
      axios.get.mockRejectedValueOnce({ message: "No pie here." });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/:name"
              component={props => (<Recipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );

      await waitForElement(
        () => dom.getByText("No pie here.", { exact: false })
      );
    });
  });
});
