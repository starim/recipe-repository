import React from 'react'
import {render} from 'react-testing-library';
import RecipeFullDetails from 'components/recipe_full_details'

describe('RecipeFullDetails', () => {

  const recipe = {
    name: "Pumpkin Pie",
    description: "An autumn dessert.",
    category: "Food",
    source: "The Internet",
    tags: [
      "Dessert",
    ],
    ingredient_preparations: [
      {
        preparation: "baked",
        amount: 9,
        unit: "in",
        ingredient: "pie crust",
      },
      {
        preparation: null,
        amount: 15,
        unit: "oz",
        ingredient: "pumpkin puree",
      },
      {
        preparation: null,
        amount: 1,
        unit: null,
        ingredient: "can of condensed milk",
      },
    ],
    directions: [
      "Mix pumpkin puree and condensed milk.",
      "Fill pie crust with pumpkin mix.",
      "Bake at 425° for 30 min.",
    ],
    comments: "Came out undercooked last time, bake for an additional five minutes.",
  };

  it("displays the recipe's name", async () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    expect(dom.getByTestId("full-details-name")).toHaveTextContent("Pumpkin Pie");
  });

  it("displays the recipe's description", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    expect(dom.getByTestId("recipe-description")).toHaveTextContent("An autumn dessert.");
  });

  it("displays the icon corresponding to the recipe's category", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    const categoryIcon = dom.container.querySelector(".category-icon");
    expect(categoryIcon.dataset.category).toBe("Food");
  });

  it("displays the recipe's source", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    expect(dom.getByTestId("recipe-source-text")).toHaveTextContent("The Internet");
  });

  it("displays the recipe's ingredients", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    const ingredientNodes =
      dom.container.querySelectorAll(".recipe-details-ingredient");
    expect(ingredientNodes.length).toBe(3);
    expect(ingredientNodes[0]).toHaveTextContent("9 in of pie crust, baked");
    expect(ingredientNodes[1]).toHaveTextContent("15 oz of pumpkin puree");
    expect(ingredientNodes[2]).toHaveTextContent("1 can of condensed milk");
  });

  it("displays the recipe's directions", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    const directionNodes =
      dom.container.querySelectorAll(".recipe-details-direction");
    expect(directionNodes.length).toBe(3);
    expect(directionNodes[0]).toHaveTextContent("Mix pumpkin puree and condensed milk.");
    expect(directionNodes[1]).toHaveTextContent("Fill pie crust with pumpkin mix.");
    expect(directionNodes[2]).toHaveTextContent("Bake at 425° for 30 min.");
  });

  it("displays the recipe's comments", () => {
    const dom = render(<RecipeFullDetails recipe={recipe} />);
    const commentSection = dom.getByText("Comments").parentNode;
    const comments = commentSection.querySelector("p");
    expect(comments).toHaveTextContent(
      "Came out undercooked last time, bake for an additional five minutes."
    );
  });
});
