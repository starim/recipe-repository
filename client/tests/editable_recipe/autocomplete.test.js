import React from 'react';
import axios from 'axios';
import {render, waitForElement, within} from 'react-testing-library';
import {MemoryRouter, Route, Switch} from 'react-router-dom';

import EditableRecipe from 'components/editable_recipe'

jest.mock('axios');
jest.spyOn(axios, "get");
jest.spyOn(axios, "post");

describe("EditableRecipe autocomplete features", () => {

  describe("when an error occurs fetching a resource list from the server", () => {
    it("displays an error to the user", async () => {
      axios.get
        .mockResolvedValueOnce({ data: [] })
        .mockRejectedValueOnce({ message: "danger danger danger" })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={["/recipes/new"]}>
          <Switch>
            <Route
              path="/recipes/new"
              component={props => (<EditableRecipe name={null} />)}
            />
          </Switch>
        </MemoryRouter>
      );

      await waitForElement(
        () => dom.getByText("danger danger danger", { exact: false })
      );
    });
  });
});
