import React from 'react';
import axios from 'axios';
import {render, fireEvent, waitForElement, within} from 'react-testing-library';
import {MemoryRouter, Route, Switch} from 'react-router-dom';

import EditableRecipe from 'components/editable_recipe'

jest.mock('axios');
jest.spyOn(axios, "get");
jest.spyOn(axios, "post");

describe("EditableRecipe form submission", () => {

  const recipeName = null;

  it("displays an error message and retains form data if the server responds with an error", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [ { name: "Food" }, { name: "Drink" } ]});
    axios.post.mockRejectedValueOnce("time for a vacation");

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const newStepButton = await waitForElement(
      () => dom.getByValue("Add New Step", { exact: false })
    );
    fireEvent.click(newStepButton);

    let inputs = await waitForElement(
      () => {
        return {
          name: dom.getByLabelText("Name", { exact: false }),
          category: dom.getByLabelText("Category", { exact: false }),
          description: dom.getByLabelText("Description", { exact: false }),
          source: dom.getByLabelText("Source", { exact: false }),
          ingredientPrep: {
            preparation: dom.getByPlaceholderText("Preparation"),
            ingredient: dom.getByPlaceholderText("Ingredient"),
            amount: dom.getByTestId("form-ingredient-preparations-amount"),
            unit: dom.getByPlaceholderText("Unit"),
          },
          directions: [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
          ],
          comments: dom.getByLabelText("Comments", { exact: false }),
        };
      }
    );

    fireEvent.change(inputs.name, {target: {value: "Pancakes"}});
    fireEvent.change(inputs.category, {target: {value: "Food"}});
    fireEvent.change(inputs.description, {target: {value: "A hearty breakfast."}});
    fireEvent.change(inputs.source, {target: {value: "A friend."}});
    fireEvent.change(inputs.ingredientPrep.amount, {target: {value: "4"}});
    fireEvent.change(inputs.ingredientPrep.unit, {target: {value: "oz"}});
    fireEvent.change(
      inputs.ingredientPrep.ingredient,
      {target: {value: "flour"}}
    );
    fireEvent.change(inputs.directions[0], {target: {value: "Heat stove."}});
    fireEvent.change(inputs.directions[1], {target: {value: "Cook ingredients."}});
    fireEvent.change(
      inputs.comments,
      {target: {value: "Try adding cinnamon in the future."}}
    );

    const submitButton = await waitForElement(
      () => dom.getByValue("Save Recipe", { exact: false })
    );
    fireEvent.click(submitButton, {});

    inputs = await waitForElement(
      () => {
        return {
          errorMessage: dom.getByText("time for a vacation", { exact: false }),
          name: dom.getByLabelText("Name", { exact: false }),
          category: dom.getByLabelText("Category", { exact: false }),
          description: dom.getByLabelText("Description", { exact: false }),
          source: dom.getByLabelText("Source", { exact: false }),
          ingredientPrep: {
            preparation: dom.getByPlaceholderText("Preparation"),
            ingredient: dom.getByPlaceholderText("Ingredient"),
            amount: dom.getByTestId("form-ingredient-preparations-amount"),
            unit: dom.getByPlaceholderText("Unit"),
          },
          directions: [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
          ],
          comments: dom.getByLabelText("Comments", { exact: false }),
        };
      }
    );

    expect(axios.post.mock.calls.length).toBe(1);
    const url = axios.post.mock.calls[0][0];
    expect(url).toMatch("/recipes");

    expect(inputs.name.value).toBe("Pancakes");
    expect(inputs.category.value).toBe("Food");
    expect(inputs.description.value).toBe("A hearty breakfast.");
    expect(inputs.source.value).toBe("A friend.");
    expect(inputs.ingredientPrep.amount.value).toBe("4");
    expect(inputs.ingredientPrep.unit.value).toBe("oz");
    expect(inputs.ingredientPrep.ingredient.value).toBe("flour");
    expect(inputs.directions[0].value).toBe("Heat stove.");
    expect(inputs.directions[1].value).toBe("Cook ingredients.");
  });

  it("allows a user to correct form errors and successuflly re-submit the form", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [ { name: "Food" }, { name: "Drink" } ]});
    axios.post.mockRejectedValueOnce("time for a vacation");
    axios.post.mockResolvedValue({});

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );

    let submitButton = await waitForElement(
      () => dom.getByValue("Save Recipe", { exact: false })
    );
    fireEvent.click(submitButton, {});

    submitButton = await waitForElement(
      () => dom.getByValue("Save Recipe", { exact: false })
    );
    fireEvent.click(submitButton, {});

    expect(axios.post.mock.calls.length).toBe(2);
  });
});
