import React from 'react';
import axios from 'axios';
import {render, fireEvent, waitForElement, within} from 'react-testing-library';
import {MemoryRouter, Route, Switch} from 'react-router-dom';

import EditableRecipe from 'components/editable_recipe'

jest.mock('axios');
jest.spyOn(axios, "get");
jest.spyOn(axios, "post");

describe("EditableRecipe when editing an existing recipe", () => {

  describe("but the server returns an error when requesting the recipe data", () => {

    const recipeName = "pie";

    it("should display the error", async () => {
      axios.get
        .mockRejectedValueOnce({ message: "No pie here." })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );

      await waitForElement(
        () => dom.getByText("No pie here.", { exact: false })
      );
    });
  });

  describe("and the server sends the recipe's data successfully", () => {

    const recipeName = "Eggs & Gravy";
    const recipe = {
      name: recipeName,
      category: "breakfast",
      description: "A filling breakfast.",
      source: "Mom",
      ingredient_preparations: [
        {
          amount: "6",
          unit: "",
          preparation: "hard-boiled",
          ingredient: "eggs",
        },
        {
          amount: "2",
          unit: "",
          preparation: "",
          ingredient: "english muffins",
        },
        {
          amount: "4",
          unit: "oz",
          preparation: "",
          ingredient: "flour",
        },
        {
          amount: null,
          unit: "",
          preparation: "",
          ingredient: "powdered sugar",
        },
      ],
      directions: [
        "Heat stove.",
        "Cook eggs.",
        "Serve over english muffins.",
      ],
      comments: "Prepare the hard-boiled eggs ahead of time.",
    };

    it("the name input defaults to the recipe's name", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Name", { exact: false })
      );
      expect(input.value).toBe("Eggs & Gravy");
    });

    it("updates the form when the recipe name is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Name", { exact: false })
      );

      fireEvent.change(input, {target: {value: "Updated Name"}});
      const updatedInput = await waitForElement(
        () => dom.getByLabelText("Name", { exact: false })
      );
      expect(updatedInput.value).toBe("Updated Name");
    });

    it("the category input defaults to the recipe's category", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [
          { name: "breakfast" },
          { name: "lunch" },
          { name: "dinner" },
        ]});

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Category", { exact: false })
      );
      expect(input.value).toBe("breakfast");
    });

    it("updates the form when the recipe category is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [
          { name: "breakfast" },
          { name: "lunch" },
          { name: "dinner" },
        ]});

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Category", { exact: false })
      );
      fireEvent.change(input, {target: {value: "lunch"}});
      expect(input.value).toBe("lunch");
    });

    it("the description input defaults to the recipe's description", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Description", { exact: false })
      );
      expect(input.value).toBe("A filling breakfast.");
    });

    it("updates the form when the recipe description is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Description", { exact: false })
      );
      fireEvent.change(input, {target: {value: "A hearty breakfast."}});
      expect(input.value).toBe("A hearty breakfast.");
    });

    it("the source input defaults to the recipe's source", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Source", { exact: false })
      );
      expect(input.value).toBe("Mom");
    });

    it("updates the form when the recipe source is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Source", { exact: false })
      );
      fireEvent.change(input, {target: {value: "My friend."}});
      expect(input.value).toBe("My friend.");
    });

    it("ingredient preparation inputs default to the recipe's ingredient preparations", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const inputs = await waitForElement(
        () => {
          return {
            preparations: dom.getAllByPlaceholderText("Preparation"),
            ingredients: dom.getAllByPlaceholderText("Ingredient"),
            amounts: dom.getAllByTestId("form-ingredient-preparations-amount"),
            units: dom.getAllByPlaceholderText("Unit"),
          };
        }
      );

      expect(inputs.preparations.map(input => input.value)).
        toEqual(["hard-boiled", "", "", ""]);
      expect(inputs.ingredients.map(input => input.value)).
        toEqual(["eggs", "english muffins", "flour", "powdered sugar"]);
      expect(inputs.amounts.map(input => input.value)).
        toEqual(["6", "2", "4", ""]);
      expect(inputs.units.map(input => input.value)).
        toEqual(["", "", "oz", ""]);
    });

    it("adds a new empty ingredient preparations input when the Add Ingredient button is clicked", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const newStepButton = await waitForElement(
        () => dom.getByValue("Add Ingredient", { exact: false })
      );
      fireEvent.click(newStepButton);
      const newInputSection = await waitForElement(
        () => {
          return dom.getByTestId("form-ingredient-preparations-input-4");
        }
      );
      const newInputs = {
        preparation: within(newInputSection).getByPlaceholderText("Preparation"),
        ingredient: within(newInputSection).getByPlaceholderText("Ingredient"),
        amount: within(newInputSection).getByTestId("form-ingredient-preparations-amount"),
        unit: within(newInputSection).getByPlaceholderText("Unit"),
      };
      expect(newInputs.preparation.value).toBe("");
      expect(newInputs.ingredient.value).toBe("");
      expect(newInputs.amount.value).toBe("1");
      expect(newInputs.unit.value).toBe("");
    });

    it("updates ingredient preparations value when the preparation input field is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByPlaceholderText("Preparation")
      );
      fireEvent.change(input, {target: {value: "roasted"}});
      expect(input.value).toBe("roasted");
    });

    it("updates ingredient preparations value when the ingredient input field is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByPlaceholderText("Ingredient")
      );
      fireEvent.change(input, {target: {value: "orange bell pepper"}});
      expect(input.value).toBe("orange bell pepper");
    });

    it("updates ingredient preparations value when the amount input field is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByTestId("form-ingredient-preparations-amount")
      );
      fireEvent.change(input, {target: {value: 1.5}});
      const updatedInput = await waitForElement(
        () => dom.getByTestId("form-ingredient-preparations-amount")
      );
      expect(updatedInput.value).toBe("1.5");
    });

    it("updates ingredient preparations value when the amount input field is set to be empty", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByTestId("form-ingredient-preparations-amount")
      );
      fireEvent.change(input, {target: {value: ""}});
      const updatedInput = await waitForElement(
        () => dom.getByTestId("form-ingredient-preparations-amount")
      );
      expect(updatedInput.value).toBe("");
    });

    it("updates ingredient preparations value when the ingredient input field is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByPlaceholderText("Unit")
      );
      fireEvent.change(input, {target: {value: "dram"}});
      expect(input.value).toBe("dram");
    });

    it("removes the correct ingredient preparation when the Remove button is clicked", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const buttonToRemoveMiddleIngredientPrep = await waitForElement(
        () => {
          return dom.getByTestId("form-ingredient-preparation-remove-1");
        }
      );
      fireEvent.click(buttonToRemoveMiddleIngredientPrep);
      const updatedInputFields = await waitForElement(
        () => {
          return {
            ingredients: dom.getAllByPlaceholderText("Ingredient"),
            preparations: dom.getAllByPlaceholderText("Preparation"),
            ingredients: dom.getAllByPlaceholderText("Ingredient"),
            amounts: dom.getAllByTestId("form-ingredient-preparations-amount"),
            units: dom.getAllByPlaceholderText("Unit"),
          };
        }
      );

      expect(updatedInputFields.ingredients.length).toBe(3);
      expect(updatedInputFields.preparations.map(input => input.value)).
        toEqual(["hard-boiled", "", ""]);
      expect(updatedInputFields.ingredients.map(input => input.value)).
        toEqual(["eggs", "flour", "powdered sugar"]);
      expect(updatedInputFields.amounts.map(input => input.value)).
        toEqual(["6", "4", ""]);
      expect(updatedInputFields.units.map(input => input.value)).
        toEqual(["", "oz", ""]);
    });

    it("directions input defaults to the recipe's directions", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const inputs = await waitForElement(
        () => {
          return [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
            dom.getByTestId("form-directions-input-2"),
          ];
        }
      );

      expect(inputs.length).toBe(3);
      expect(inputs[0].value).toBe("Heat stove.");
      expect(inputs[1].value).toBe("Cook eggs.");
      expect(inputs[2].value).toBe("Serve over english muffins.");
    });

    it("adds a new empty directions input when the Add New Step button is clicked", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const newStepButton = await waitForElement(
        () => dom.getByValue("Add New Step", { exact: false })
      );
      fireEvent.click(newStepButton);
      const input = await waitForElement(
        () => dom.getByTestId("form-directions-input-3")
      );
      expect(input.value).toBe("");
    });

    it("updates directions value when an input field is changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByTestId("form-directions-input-0")
      );
      fireEvent.change(input, {target: {value: "Preheat oven to 425°"}});
      expect(input.value).toBe("Preheat oven to 425°");
    });

    it("removes the correct direction when the Remove button is clicked", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );

      const buttonToRemoveMiddleDirection = await waitForElement(
        () => {
          return dom.getByTestId("form-directions-remove-1");
        }
      );
      fireEvent.click(buttonToRemoveMiddleDirection);
      const updatedInputFields = await waitForElement(
        () => {
          return [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
          ];
        }
      );
      expect(updatedInputFields.length).toBe(2);
      expect(updatedInputFields[0].value).toBe("Heat stove.");
      expect(updatedInputFields[1].value).toBe("Serve over english muffins.");
    });

    it("comment input defaults to the recipe's comments", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Comments", { exact: false })
      );
      expect(input.value).toBe("Prepare the hard-boiled eggs ahead of time.");
    });

    it("updates the form when the recipe comments are changed", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] });

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
          </Switch>
        </MemoryRouter>
      );
      const input = await waitForElement(
        () => dom.getByLabelText("Comments", { exact: false })
      );

      fireEvent.change(input, {target: {value: "Updated Comments"}});
      const updatedInput = await waitForElement(
        () => dom.getByLabelText("Comments", { exact: false })
      );
      expect(updatedInput.value).toBe("Updated Comments");
    });

    it("sends all recipe data correctly formatted when the Submit button is clicked and then takes the user to the recipe#show page", async () => {
      axios.get
        .mockResolvedValueOnce({ data: recipe })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [] })
        .mockResolvedValueOnce({ data: [{ name: "Food" }, { name: "Drink" }]});
      axios.put.mockResolvedValue({});

      const MockShowPage = (props) => {
        const name = props.match.params.name;

        return (
          <div data-testid="urlName">{name}</div>
        );
      };

      const dom = render(
        <MemoryRouter initialEntries={[`/recipes/edit/${recipeName}`]}>
          <Switch>
            <Route
              path="/recipes/edit/:name"
              component={props => (<EditableRecipe name={recipeName} />)}
            />
            <Route path="/recipes/:name" component={MockShowPage} />
          </Switch>
        </MemoryRouter>
      );

      const newIngredientButton = await waitForElement(
        () => dom.getByValue("Add Ingredient", { exact: false })
      );
      fireEvent.click(newIngredientButton);
      const ingredPrepSections = await waitForElement(
        () => {
          return [
            dom.getByTestId("form-ingredient-preparations-input-0"),
            dom.getByTestId("form-ingredient-preparations-input-1"),
            dom.getByTestId("form-ingredient-preparations-input-2"),
            dom.getByTestId("form-ingredient-preparations-input-3"),
            dom.getByTestId("form-ingredient-preparations-input-4"),
          ];
        }
      );

      const newStepButton = await waitForElement(
        () => dom.getByValue("Add New Step", { exact: false })
      );
      fireEvent.click(newStepButton);

      const inputs = await waitForElement(
        () => {
          return {
            name: dom.getByLabelText("Name", { exact: false }),
            category: dom.getByLabelText("Category", { exact: false }),
            description: dom.getByLabelText("Description", { exact: false }),
            source: dom.getByLabelText("Source", { exact: false }),
            ingredientPreparations: ingredPrepSections.map(thisInputSection => {
              const scope = within(thisInputSection);
              const preparation = scope.getByPlaceholderText("Preparation");
              const ingredient = scope.getByPlaceholderText("Ingredient");
              const amount = scope.getByTestId("form-ingredient-preparations-amount");
              const unit = scope.getByPlaceholderText("Unit");

              return {
                preparation,
                ingredient,
                amount,
                unit,
              };
            }),
            directions: [
              dom.getByTestId("form-directions-input-0"),
              dom.getByTestId("form-directions-input-1"),
              dom.getByTestId("form-directions-input-2"),
              dom.getByTestId("form-directions-input-3"),
            ],
            comments: dom.getByLabelText("Comments", { exact: false }),
          };
        }
      );

      fireEvent.change(inputs.name, {target: {value: "Pancakes"}});
      fireEvent.change(inputs.category, {target: {value: "Food"}});
      fireEvent.change(inputs.description, {target: {value: "A hearty breakfast."}});
      fireEvent.change(inputs.source, {target: {value: "A friend."}});
      fireEvent.change(
        inputs.ingredientPreparations[2].amount,
        {target: {value: "4"}}
      );
      fireEvent.change(
        inputs.ingredientPreparations[2].unit,
        {target: {value: "oz"}}
      );
      fireEvent.change(
        inputs.ingredientPreparations[2].ingredient,
        {target: {value: "flour"}}
      );
      fireEvent.change(
        inputs.ingredientPreparations[4].preparation,
        {target: {value: "sliced"}}
      );
      fireEvent.change(
        inputs.ingredientPreparations[4].amount,
        {target: {value: "0.5"}}
      );
      fireEvent.change(
        inputs.ingredientPreparations[4].ingredient,
        {target: {value: "apples"}}
      );
      fireEvent.change(inputs.directions[0], {target: {value: "Heat stove."}});
      fireEvent.change(inputs.directions[1], {target: {value: "Cook ingredients."}});
      fireEvent.change(inputs.directions[2], {target: {value: "Wait for pancakes to cool."}});
      fireEvent.change(inputs.directions[3], {target: {value: "Serve over syrup."}});
      fireEvent.change(
        inputs.comments,
        {target: {value: "Try adding cinnamon in the future."}}
      );

      const submitButton = await waitForElement(
        () => dom.getByValue("Save Recipe", { exact: false })
      );
      fireEvent.click(submitButton, {});
      const nameInUrl = await waitForElement(
        () => dom.getByTestId("urlName")
      );
      expect(nameInUrl).toHaveTextContent("Pancakes");
      expect(axios.put.mock.calls.length).toBe(1);
      const url = axios.put.mock.calls[0][0];
      const formContents = axios.put.mock.calls[0][1];
      expect(url).toMatch("/recipes/Eggs%20%26%20Gravy");
      expect(formContents).toEqual({
        recipe: {
          name: "Pancakes",
          description: "A hearty breakfast.",
          source: "A friend.",
          ingredient_preparations: [
            {
              ingredient: "eggs",
              preparation: "hard-boiled",
              amount: 6,
              unit: "",
            },
            {
              ingredient: "english muffins",
              preparation: "",
              amount: 2,
              unit: "",
            },
            {
              ingredient: "flour",
              preparation: "",
              amount: 4,
              unit: "oz",
            },
            {
              ingredient: "powdered sugar",
              preparation: "",
              amount: null,
              unit: "",
            },
            {
              ingredient: "apples",
              preparation: "sliced",
              amount: 0.5,
              unit: "",
            },
          ],
          directions: [
            "Heat stove.",
            "Cook ingredients.",
            "Wait for pancakes to cool.",
            "Serve over syrup.",
          ],
          comments: "Try adding cinnamon in the future.",
          category: "Food",
        }
      });
    });
  });
});
