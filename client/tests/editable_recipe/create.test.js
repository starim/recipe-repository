import React from 'react';
import axios from 'axios';
import {render, fireEvent, waitForElement, within} from 'react-testing-library';
import {MemoryRouter, Route, Switch} from 'react-router-dom';

import EditableRecipe from 'components/editable_recipe'

jest.mock('axios');
jest.spyOn(axios, "get");
jest.spyOn(axios, "post");

describe("EditableRecipe when creating a new recipe", () => {

  const recipeName = null;

  it("defaults to a blank name", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Name", { exact: false })
    );
    expect(input.value).toBe("");
  });

  it("updates the form when the recipe name is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Name", { exact: false })
    );

    fireEvent.change(input, {target: {value: "Updated Name"}});
    const updatedInput = await waitForElement(
      () => dom.getByLabelText("Name", { exact: false })
    );
    expect(updatedInput.value).toBe("Updated Name");
  });

  it("defaults to a blank category", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [
        { name: "breakfast" },
        { name: "lunch" },
        { name: "dinner" },
      ]});

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Category", { exact: false })
    );
    expect(input.value).toBe("");
  });

  it("updates the form when the recipe category is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [
        { name: "breakfast" },
        { name: "lunch" },
        { name: "dinner" },
      ]});

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Category", { exact: false })
    );
    fireEvent.change(input, {target: {value: "lunch"}});
    expect(input.value).toBe("lunch");
  });

  it("defaults to a blank description", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Description", { exact: false })
    );
    expect(input.value).toBe("");
  });

  it("updates the form when the recipe description is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Description", { exact: false })
    );
    fireEvent.change(input, {target: {value: "A hearty breakfast."}});
    expect(input.value).toBe("A hearty breakfast.");
  });

  it("defaults to a blank source", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Source", { exact: false })
    );
    expect(input.value).toBe("");
  });

  it("updates the form when the recipe source is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Source", { exact: false })
    );
    fireEvent.change(input, {target: {value: "My friend."}});
    expect(input.value).toBe("My friend.");
  });

  it("defaults to a single empty ingredient preparations input", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const inputs = await waitForElement(
      () => {
        return {
          preparation: dom.getByPlaceholderText("Preparation"),
          ingredient: dom.getByPlaceholderText("Ingredient"),
          amount: dom.getByTestId("form-ingredient-preparations-amount"),
          unit: dom.getByPlaceholderText("Unit"),
        };
      }
    );
    expect(inputs.preparation.value).toBe("");
    expect(inputs.ingredient.value).toBe("");
    expect(inputs.amount.value).toBe("1");
    expect(inputs.unit.value).toBe("");
  });

  it("adds a new empty ingredient preparations input when the Add New Step button is clicked", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const newStepButton = await waitForElement(
      () => dom.getByValue("Add Ingredient", { exact: false })
    );
    fireEvent.click(newStepButton);
    const newInputSection = await waitForElement(
      () => {
        return dom.getByTestId("form-ingredient-preparations-input-1");
      }
    );
    const newInputs = {
      preparation: within(newInputSection).getByPlaceholderText("Preparation"),
      ingredient: within(newInputSection).getByPlaceholderText("Ingredient"),
      amount: within(newInputSection).getByTestId("form-ingredient-preparations-amount"),
      unit: within(newInputSection).getByPlaceholderText("Unit"),
    };
    expect(newInputs.preparation.value).toBe("");
    expect(newInputs.ingredient.value).toBe("");
    expect(newInputs.amount.value).toBe("1");
    expect(newInputs.unit.value).toBe("");
  });

  it("updates ingredient preparations value when the preparation input field is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByPlaceholderText("Preparation")
    );
    fireEvent.change(input, {target: {value: "roasted"}});
    expect(input.value).toBe("roasted");
  });

  it("updates ingredient preparations value when the ingredient input field is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByPlaceholderText("Ingredient")
    );
    fireEvent.change(input, {target: {value: "orange bell pepper"}});
    expect(input.value).toBe("orange bell pepper");
  });

  it("updates ingredient preparations value when the amount input field is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByTestId("form-ingredient-preparations-amount")
    );
    fireEvent.change(input, {target: {value: 1.5}});
    const updatedInput = await waitForElement(
      () => dom.getByTestId("form-ingredient-preparations-amount")
    );
    expect(updatedInput.value).toBe("1.5");
  });

  it("updates ingredient preparations value when the amount input field is set to be empty", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByTestId("form-ingredient-preparations-amount")
    );
    fireEvent.change(input, {target: {value: ""}});
    const updatedInput = await waitForElement(
      () => dom.getByTestId("form-ingredient-preparations-amount")
    );
    expect(updatedInput.value).toBe("");
  });

  it("updates ingredient preparations value when the ingredient input field is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByPlaceholderText("Unit")
    );
    fireEvent.change(input, {target: {value: "dram"}});
    expect(input.value).toBe("dram");
  });

  it("removes the correct ingredient preparation when the Remove button is clicked", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const addNewIngredientPreparation = async (dom) => {
      const newStepButton = await waitForElement(
        () => dom.getByValue("Add Ingredient")
      );
      fireEvent.click(newStepButton);
    };

    const fillInIngredientPreparations = async (dom) => {
      const ingredientInputs = await waitForElement(
        () => {
          return dom.getAllByPlaceholderText("Ingredient");
        }
      );
      fireEvent.change(ingredientInputs[0], {target: {value: "eggs"}});
      fireEvent.change(ingredientInputs[1], {target: {value: "tortilla"}});
      fireEvent.change(ingredientInputs[2], {target: {value: "chorizo"}});
    };

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    addNewIngredientPreparation(dom);
    addNewIngredientPreparation(dom);
    fillInIngredientPreparations(dom);

    const buttonToRemoveMiddleIngredientPrep = await waitForElement(
      () => {
        return dom.getByTestId("form-ingredient-preparation-remove-1");
      }
    );
    fireEvent.click(buttonToRemoveMiddleIngredientPrep);
    const updatedInputFields = await waitForElement(
      () => dom.getAllByPlaceholderText("Ingredient")
    );
    expect(updatedInputFields.length).toBe(2);
    expect(updatedInputFields[0].value).toBe("eggs");
    expect(updatedInputFields[1].value).toBe("chorizo");
  });

  it("defaults to a single empty directions input", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByTestId("form-directions-input-0")
    );
    expect(input.value).toBe("");
  });

  it("adds a new empty directions input when the Add New Step button is clicked", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const newStepButton = await waitForElement(
      () => dom.getByValue("Add New Step", { exact: false })
    );
    fireEvent.click(newStepButton);
    const input = await waitForElement(
      () => dom.getByTestId("form-directions-input-1")
    );
    expect(input.value).toBe("");
  });

  it("updates directions value when an input field is changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByTestId("form-directions-input-0")
    );
    fireEvent.change(input, {target: {value: "Preheat oven to 425°"}});
    expect(input.value).toBe("Preheat oven to 425°");
  });

  it("removes the correct direction when the Remove button is clicked", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const addNewDirection = async (dom) => {
      const newStepButton = await waitForElement(
        () => dom.getByValue("Add New Step")
      );
      fireEvent.click(newStepButton);
    };

    const fillInSteps = async (dom) => {
      const inputFields = await waitForElement(
        () => [
          dom.getByTestId("form-directions-input-0"),
          dom.getByTestId("form-directions-input-1"),
          dom.getByTestId("form-directions-input-2"),
        ]
      );
      fireEvent.change(inputFields[0], {target: {value: "Preheat oven to 425°."}});
      fireEvent.change(inputFields[1], {target: {value: "Shred cheese."}});
      fireEvent.change(inputFields[2], {target: {value: "Thaw sausage."}});
    };

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    addNewDirection(dom);
    addNewDirection(dom);
    fillInSteps(dom);

    const buttonToRemoveMiddleDirection = await waitForElement(
      () => {
        return dom.getByTestId("form-directions-remove-1");
      }
    );
    fireEvent.click(buttonToRemoveMiddleDirection);
    const updatedInputFields = await waitForElement(
      () => {
        return [
          dom.getByTestId("form-directions-input-0"),
          dom.getByTestId("form-directions-input-1"),
        ];
      }
    );
    expect(updatedInputFields.length).toBe(2);
    expect(updatedInputFields[0].value).toBe("Preheat oven to 425°.");
    expect(updatedInputFields[1].value).toBe("Thaw sausage.");
  });

  it("defaults to blank comments", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Comments", { exact: false })
    );
    expect(input.value).toBe("");
  });

  it("updates the form when the recipe comments are changed", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] });

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const input = await waitForElement(
      () => dom.getByLabelText("Comments", { exact: false })
    );

    fireEvent.change(input, {target: {value: "Updated Comments"}});
    const updatedInput = await waitForElement(
      () => dom.getByLabelText("Comments", { exact: false })
    );
    expect(updatedInput.value).toBe("Updated Comments");
  });

  it("sends all recipe data correctly formatted when the Submit button is clicked and then takes the user to the recipe#show page", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [ { name: "Food" }, { name: "Drink" } ]});
    axios.post.mockResolvedValue({});

    const MockShowPage = (props) => {
      const name = props.match.params.name;

      return (
        <div data-testid="urlName">{name}</div>
      );
    };

    const dom = render(<MemoryRouter initialEntries={["/recipes/new"]}>
      <Switch>
        <Route
          path="/recipes/new"
          component={props => (<EditableRecipe name={recipeName} />)}
        />
        <Route path="/recipes/:name" component={MockShowPage} />
      </Switch>
    </MemoryRouter>);
    const newIngredientButton = await waitForElement(
      () => dom.getByValue("Add Ingredient", { exact: false })
    );
    fireEvent.click(newIngredientButton);
    const ingredPrepSections = await waitForElement(
      () => {
        return [
          dom.getByTestId("form-ingredient-preparations-input-0"),
          dom.getByTestId("form-ingredient-preparations-input-1"),
        ];
      }
    );
    const newStepButton = await waitForElement(
      () => dom.getByValue("Add New Step", { exact: false })
    );
    fireEvent.click(newStepButton);
    fireEvent.click(newStepButton);

    const inputs = await waitForElement(
      () => {
        return {
          name: dom.getByLabelText("Name", { exact: false }),
          category: dom.getByLabelText("Category", { exact: false }),
          description: dom.getByLabelText("Description", { exact: false }),
          source: dom.getByLabelText("Source", { exact: false }),
          ingredientPreparations: ingredPrepSections.map(thisInputSection => {
            const scope = within(thisInputSection);
            const preparation = scope.getByPlaceholderText("Preparation");
            const ingredient = scope.getByPlaceholderText("Ingredient");
            const amount = scope.getByTestId("form-ingredient-preparations-amount");
            const unit = scope.getByPlaceholderText("Unit");

            return {
              preparation,
              ingredient,
              amount,
              unit,
            };
          }),
          directions: [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
            dom.getByTestId("form-directions-input-2"),
          ],
          comments: dom.getByLabelText("Comments", { exact: false }),
        };
      }
    );

    fireEvent.change(inputs.name, {target: {value: "Pancakes"}});
    fireEvent.change(inputs.category, {target: {value: "Food"}});
    fireEvent.change(inputs.description, {target: {value: "A hearty breakfast."}});
    fireEvent.change(inputs.source, {target: {value: "A friend."}});
    fireEvent.change(
      inputs.ingredientPreparations[0].amount,
      {target: {value: "4"}}
    );
    fireEvent.change(
      inputs.ingredientPreparations[0].unit,
      {target: {value: "oz"}}
    );
    fireEvent.change(
      inputs.ingredientPreparations[0].ingredient,
      {target: {value: "flour"}}
    );
    fireEvent.change(
      inputs.ingredientPreparations[1].preparation,
      {target: {value: "sliced"}}
    );
    fireEvent.change(
      inputs.ingredientPreparations[1].amount,
      {target: {value: ""}}
    );
    fireEvent.change(
      inputs.ingredientPreparations[1].ingredient,
      {target: {value: "apples"}}
    );
    fireEvent.change(inputs.directions[0], {target: {value: "Heat stove."}});
    fireEvent.change(inputs.directions[1], {target: {value: "Cook ingredients."}});
    fireEvent.change(inputs.directions[2], {target: {value: "Let cool."}});
    fireEvent.change(
      inputs.comments,
      {target: {value: "Try adding cinnamon in the future."}}
    );

    const submitButton = await waitForElement(
      () => dom.getByValue("Save Recipe", { exact: false })
    );
    fireEvent.click(submitButton, {});
    const nameInUrl = await waitForElement(
      () => dom.getByTestId("urlName")
    );
    expect(nameInUrl).toHaveTextContent("Pancakes");
    expect(axios.post.mock.calls.length).toBe(1);
    const url = axios.post.mock.calls[0][0];
    const formContents = axios.post.mock.calls[0][1];
    expect(url).toMatch("/recipes");
    expect(formContents).toEqual({
      recipe: {
        name: "Pancakes",
        description: "A hearty breakfast.",
        source: "A friend.",
        ingredient_preparations: [
          {
            ingredient: "flour",
            preparation: "",
            amount: 4,
            unit: "oz",
          },
          {
            ingredient: "apples",
            preparation: "sliced",
            amount: null,
            unit: "",
          },
        ],
        tags: [""],
        directions: [
          "Heat stove.",
          "Cook ingredients.",
          "Let cool.",
        ],
        comments: "Try adding cinnamon in the future.",
        category: "Food",
      }
    });
  });

  it("displays an error message and retains form data if the server responds with an error", async () => {
    axios.get
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [] })
      .mockResolvedValueOnce({ data: [ { name: "Food" }, { name: "Drink" } ]});
    axios.post.mockRejectedValue("time for a vacation");

    const dom = render(
      <MemoryRouter initialEntries={["/recipes/new"]}>
        <Switch>
          <Route
            path="/recipes/new"
            component={props => (<EditableRecipe name={recipeName} />)}
          />
        </Switch>
      </MemoryRouter>
    );
    const newStepButton = await waitForElement(
      () => dom.getByValue("Add New Step", { exact: false })
    );
    fireEvent.click(newStepButton);

    let inputs = await waitForElement(
      () => {
        return {
          name: dom.getByLabelText("Name", { exact: false }),
          category: dom.getByLabelText("Category", { exact: false }),
          description: dom.getByLabelText("Description", { exact: false }),
          source: dom.getByLabelText("Source", { exact: false }),
          ingredientPrep: {
            preparation: dom.getByPlaceholderText("Preparation"),
            ingredient: dom.getByPlaceholderText("Ingredient"),
            amount: dom.getByTestId("form-ingredient-preparations-amount"),
            unit: dom.getByPlaceholderText("Unit"),
          },
          directions: [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
          ],
          comments: dom.getByLabelText("Comments", { exact: false }),
        };
      }
    );

    fireEvent.change(inputs.name, {target: {value: "Pancakes"}});
    fireEvent.change(inputs.category, {target: {value: "Food"}});
    fireEvent.change(inputs.description, {target: {value: "A hearty breakfast."}});
    fireEvent.change(inputs.source, {target: {value: "A friend."}});
    fireEvent.change(inputs.ingredientPrep.amount, {target: {value: "4"}});
    fireEvent.change(inputs.ingredientPrep.unit, {target: {value: "oz"}});
    fireEvent.change(
      inputs.ingredientPrep.ingredient,
      {target: {value: "flour"}}
    );
    fireEvent.change(inputs.directions[0], {target: {value: "Heat stove."}});
    fireEvent.change(inputs.directions[1], {target: {value: "Cook ingredients."}});
    fireEvent.change(
      inputs.comments,
      {target: {value: "Try adding cinnamon in the future."}}
    );

    const submitButton = await waitForElement(
      () => dom.getByValue("Save Recipe", { exact: false })
    );
    fireEvent.click(submitButton, {});

    inputs = await waitForElement(
      () => {
        return {
          errorMessage: dom.getByText("time for a vacation", { exact: false }),
          name: dom.getByLabelText("Name", { exact: false }),
          category: dom.getByLabelText("Category", { exact: false }),
          description: dom.getByLabelText("Description", { exact: false }),
          source: dom.getByLabelText("Source", { exact: false }),
          ingredientPrep: {
            preparation: dom.getByPlaceholderText("Preparation"),
            ingredient: dom.getByPlaceholderText("Ingredient"),
            amount: dom.getByTestId("form-ingredient-preparations-amount"),
            unit: dom.getByPlaceholderText("Unit"),
          },
          directions: [
            dom.getByTestId("form-directions-input-0"),
            dom.getByTestId("form-directions-input-1"),
          ],
          comments: dom.getByLabelText("Comments", { exact: false }),
        };
      }
    );

    expect(axios.post.mock.calls.length).toBe(1);
    const url = axios.post.mock.calls[0][0];
    expect(url).toMatch("/recipes");

    expect(inputs.name.value).toBe("Pancakes");
    expect(inputs.category.value).toBe("Food");
    expect(inputs.description.value).toBe("A hearty breakfast.");
    expect(inputs.source.value).toBe("A friend.");
    expect(inputs.ingredientPrep.amount.value).toBe("4");
    expect(inputs.ingredientPrep.unit.value).toBe("oz");
    expect(inputs.ingredientPrep.ingredient.value).toBe("flour");
    expect(inputs.directions[0].value).toBe("Heat stove.");
    expect(inputs.directions[1].value).toBe("Cook ingredients.");
  });
});
