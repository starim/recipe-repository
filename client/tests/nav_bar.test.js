import React from 'react';
import {render, waitForElement, within} from 'react-testing-library';
import {Router, Route, Switch} from 'react-router-dom';
import {createMemoryHistory} from "history";

import NavBar from 'components/nav_bar/nav_bar';

describe("Nav bar", () => {

  describe("when the route is /recipes", () => {
  const route = "/recipes";

  it("shows the All Recipes link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("All Recipes")).not.toBeNull();
  });

  it("shows the New Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("New Recipe")).not.toBeNull();
  });

  it("doesn't show the Edit Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.queryByText("Edit Recipe")).toBeNull();
  });
  });

  describe("when the route is /recipes/new", () => {
  const route = "/recipes/new";

  it("shows the All Recipes link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("All Recipes")).not.toBeNull();
  });

  it("shows the New Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("New Recipe")).not.toBeNull();
  });

  it("doesn't show the Edit Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.queryByText("Edit Recipe")).toBeNull();
  });
  });

  describe("when the route is /recipes/<name>", () => {
  const route = "/recipes/ffnaff";

  it("shows the All Recipes link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("All Recipes")).not.toBeNull();
  });

  it("shows the New Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("New Recipe")).not.toBeNull();
  });

  it("shows the Edit Recipe link", () => {
    const history = createMemoryHistory({
    initialEntries: [route],
    });

    const dom = render(
    <Router history={history}>
      <NavBar />
    </Router>
    );

    expect(dom.getByText("Edit Recipe")).not.toBeNull();
  });
  });
});
