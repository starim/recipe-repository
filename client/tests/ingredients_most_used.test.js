import React from 'react'
import axios from 'axios';
import {MemoryRouter, Router} from 'react-router-dom';
import {render, fireEvent, waitForElement, waitForDomChange, within} from 'react-testing-library';
import {createMemoryHistory} from "history";

import IngredientsMostUsed from 'components/ingredients_most_used';
import App from 'app';

const ingredients = [
  { id: 1, name: "Egg", num_recipes: 40 },
  { id: 2, name: "Tortilla", num_recipes: 36 },
  { id: 3, name: "Cheddar Cheese", num_recipes: 28 },
  { id: 4, name: "Lettuce", num_recipes: 22 },
  { id: 5, name: "Serrano Pepper", num_recipes: 1 },
];

jest.mock("axios");
jest.spyOn(axios, "get");

describe("IngredientsMostUsed", () => {

  describe("when the server returns the most-used ingredients list successfully", () => {

    beforeEach(() => {
      axios.get.mockResolvedValueOnce({ data: ingredients });
    });

    it("displays the ingredients' names in order from most-used to least", async () => {
      const dom = render(<MemoryRouter><IngredientsMostUsed /></MemoryRouter>);
      const displayedNames  = await waitForElement(
        () => dom.getAllByTestId("card-name")
      );
      expect(displayedNames.length).toBe(5);
      expect(displayedNames[0]).toHaveTextContent("Egg");
      expect(displayedNames[1]).toHaveTextContent("Tortilla");
      expect(displayedNames[2]).toHaveTextContent("Cheddar Cheese");
      expect(displayedNames[3]).toHaveTextContent("Lettuce");
      expect(displayedNames[4]).toHaveTextContent("Serrano Pepper");
    });

    it("displays the number of recipes each ingredient is used in", async () => {
      const dom = render(<MemoryRouter><IngredientsMostUsed /></MemoryRouter>);
      const displayedRecipeCount  = await waitForElement(
        () => dom.getAllByTestId("card-recipe-count")
      );
      expect(displayedRecipeCount.length).toBe(5);
      expect(displayedRecipeCount[0]).toHaveTextContent("40 recipes");
      expect(displayedRecipeCount[1]).toHaveTextContent("36 recipes");
      expect(displayedRecipeCount[2]).toHaveTextContent("28 recipes");
      expect(displayedRecipeCount[3]).toHaveTextContent("22 recipes");
      expect(displayedRecipeCount[4]).toHaveTextContent("1 recipe");
    });

    it("links to the recipe list for each ingredient", async () => {

      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/ingredients/most-used"],
      });
      const dom = render(<Router history={history}><App /></Router>);
      const ingredientCards  = await waitForElement(
        () => dom.getAllByTestId("card-link")
      );
      expect(ingredientCards.length).toBe(5);
      fireEvent.click(ingredientCards[1], {});
      await waitForDomChange();

      expect(history.location.pathname).toEqual("/recipes");
      expect(history.location.search).toEqual("?ingredient=Tortilla");
    });
  });

  describe("when there's an error fetching the most-used ingredients", () => {
    it("should display the error", async () => {
      axios.get.mockRejectedValueOnce({ message: "server's on fire" });
      const dom = render(<MemoryRouter><IngredientsMostUsed /></MemoryRouter>);

      await waitForElement(
        () => dom.getByText("server's on fire", { exact: false })
      );
    });
  });
});
