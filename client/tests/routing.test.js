import React from 'react';
import axios from 'axios';
import {render, waitForElement, within} from 'react-testing-library';
import {Router, Route, Switch} from 'react-router-dom';
import {createMemoryHistory} from "history";

import App from 'app';

jest.mock('axios');
jest.spyOn(axios, "get");

describe("App routing", () => {

  describe("when the route is /", () => {
    it("redirects to the recipes index page", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      await waitForElement(
        () => dom.getByTestId("recipe-index")
      );
      expect(history.location.pathname).toBe("/recipes");
    });
  });

  describe("when the route is /recipes", () => {
    it("shows the recipes index page", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      await waitForElement(
        () => dom.getByTestId("recipe-index")
      );
      expect(history.location.pathname).toBe("/recipes");
    });
  });

  describe("when the route is /recipes/new", () => {
    it("shows the new recipe form", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/recipes/new"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      await waitForElement(
        () => dom.getByTestId("recipe-form")
      );

      expect(history.location.pathname).toBe("/recipes/new");
    });
  });

  describe("when the route is /recipes/<name>", () => {
    it("shows the recipe detail page", async () => {
      const recipe = {
      name: "pie",
      category: "",
      description: "",
      source: "",
      tags: [""],
      ingredient_preparations: [],
      directions: [],
      comments: "Prepare the hard-boiled eggs ahead of time.",
      };
      axios.get.mockResolvedValue({ data: recipe });

      const history = createMemoryHistory({
      initialEntries: ["/recipes/pie"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      await waitForElement(
        () => dom.getByTestId("recipe-show")
      );
      expect(history.location.pathname).toBe("/recipes/pie");
    });
  });

  describe("when the route is /recipes/edit/pie", () => {
    it("shows the new recipe form", async () => {
      const recipe = {
      name: "pie",
      category: "",
      description: "",
      source: "",
      ingredient_preparations: [],
      directions: [],
      comments: "Prepare the hard-boiled eggs ahead of time.",
      };
      axios.get
        .mockResolvedValue({ data: [] })
        .mockResolvedValueOnce({ data: recipe });

      const history = createMemoryHistory({
        initialEntries: ["/recipes/edit/pie"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );

      await waitForElement(
        () => dom.getByTestId("recipe-form")
      );
      expect(history.location.pathname).toBe("/recipes/edit/pie");
    });
  });

  describe("when the route isn't a recognized route", () => {
    it("redirects to the recipes index page", async () => {
      axios.get.mockResolvedValue({ data: [] });

      const history = createMemoryHistory({
        initialEntries: ["/fnaffnaf"],
      });

      const dom = render(
        <Router history={history}>
          <App />
        </Router>
      );


      await waitForElement(
        () => dom.getByTestId("recipe-index")
      );
      expect(history.location.pathname).toBe("/recipes");
    });
  });
});
